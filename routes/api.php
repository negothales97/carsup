<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('session','Api\SessionController@verifySession')->name('api.session');
Route::get('session/set','Api\SessionController@setSession')->name('api.session.set');
Route::get('bank','Api\BankController@index')->name('api.bank.index');
Route::post('institute','Api\InstituteController@index');
