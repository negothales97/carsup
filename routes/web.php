<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'Customer\HomeController@index');
Route::get('/localizacao', 'Customer\LocationController@index')->name('location');
Route::get('/estabelecimentos', 'Customer\InstituteController@index')->name('institute.index');
Route::get('/estabelecimento/{resourceId}', 'Customer\InstituteController@show')->name('institute.show');

Route::group(['as' => 'page.'], function () {
    Route::get('/quem-somos', 'Customer\PageController@about')->name('about');
    Route::get('/politicas-de-privacidade', 'Customer\PageController@policy')->name('policy');
    Route::get('/termos-de-uso', 'Customer\PageController@term')->name('term');
    Route::get('/fale-conosco', 'Customer\PageController@contact')->name('contact');
    Route::get('/perguntas-frequentes', 'Customer\PageController@commonQuestion')->name('common.question');
});
Route::group(['as' => 'register.'], function () {
    Route::group(['as' => 'step.'], function () {
        Route::get('/passo-1', 'Customer\RegisterController@firstStep')->name('first');
        Route::get('/passo-2', 'Customer\RegisterController@secondStep')->name('second');
        Route::get('/passo-3', 'Customer\RegisterController@thirdStep')->name('third');
        Route::get('/passo-4', 'Customer\RegisterController@fourthStep')->name('fourth');
        Route::get('/passo-5', 'Customer\RegisterController@fifthStep')->name('fifth');
    });
    Route::group(['as' => 'form.'], function () {
        Route::post('/passo-1', 'Customer\RegisterController@firstRegister')->name('first');
        Route::post('/passo-2', 'Customer\RegisterController@secondRegister')->name('second');
        Route::post('/passo-3', 'Customer\RegisterController@thirdRegister')->name('third');
        Route::post('/passo-4', 'Customer\RegisterController@fourthRegister')->name('fourth');
        Route::post('/passo-5', 'Customer\RegisterController@fifthRegister')->name('fifth');
    });
});
