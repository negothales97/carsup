<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function verifySession()
    {
        $location = session('location');
        return $location ? "true" : "false";
    }

    public function setSession(Request $request)
    {

        $url =
            "https://maps.googleapis.com/maps/api/geocode/json?&key=AIzaSyDXruFBpALszeLY69jhYiZGjXsnszWSyf0&latlng={$request->location['latitude']},{$request->location['longitude']}&";
        $data = [
            "libraries" => "places",
            'language' => 'pt-BR',
        ];
        $data = http_build_query($data);
        $url .= $data;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $json = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($json);
        $latitude = $result->results[0]->geometry->location->lat;
        $longitude = $result->results[0]->geometry->location->lng;
        $address = $result->results[0]->address_components[2]->long_name;

        return response()->json([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'address' => $address,
        ], 201);
    }
}
