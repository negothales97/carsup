<?php

namespace App\Http\Controllers\Api;

use App\Models\Institute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstituteController extends Controller
{
    public function index(Request $request)
    {
        $location = json_decode($request->location);
        $api_url = config('services.api.url');
        $responseInstitutes = requestCurl($api_url . "carwashes/nearby?lat={$location->latitude}&lng={$location->longitude}") ?? [];
        $institutes = Institute::hydrate($responseInstitutes)->where('active', true);

        return view('components.institute')
            ->with('institutes', $institutes);
    }
}
