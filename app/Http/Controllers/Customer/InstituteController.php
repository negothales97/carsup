<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Institute;

class InstituteController extends Controller
{
    public function index()
    {
        return view('customer.pages.institute.index');
    }
    public function show($resourceId)
    {
        $api_url = config('services.api.url');
        $response = requestCurl($api_url . 'carwash?id=' . $resourceId) ?? [];
        $responseServices = requestCurl($api_url . 'carwash/services?id=' . $resourceId) ?? [];
        return view('customer.pages.institute.show')
        ->with('institute', $response->data)
        ->with('services', $responseServices);
    }
}
