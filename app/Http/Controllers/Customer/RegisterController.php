<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    public function firstStep()
    {
        return view('customer.pages.register.step-1');
    }
    public function secondStep()
    {
        return view('customer.pages.register.step-2');
    }
    public function thirdStep()
    {
        return view('customer.pages.register.step-3');
    }
    public function fourthStep()
    {
        return view('customer.pages.register.step-4');
    }
    public function fifthStep()
    {
        return view('customer.pages.register.step-5');
    }

    public function firstRegister(RegisterRequest $request)
    {
        $owner = $request->except('_token');
        session(['owner' => $owner]);
        return redirect()->route('register.step.second');
    }
    public function secondRegister(RegisterRequest $request)
    {
        $carWash = $request->except('_token');
        // $dataPosition = [
        //     'geohash' => ["stringValue" => $geoHash],
        //     'geopoint' => ["geoPointValue" => (object) ["latitude" => $location->lat, "longitude" =>  $location->lng]]
        // ];

        // session(['position' => (object) $dataPosition]);
        session(['carWash' => $carWash]);
        return redirect()->route('register.step.third');
    }
    public function thirdRegister(RegisterRequest $request)
    {
        $bank = $request->except('_token');
        session(['bank' => $bank]);
        return redirect()->route('register.step.fourth');
    }
    public function fourthRegister(RegisterRequest $request)
    {
        if ($request->seguroPossui == "true") {
            $other = $request->except('_token', 'seguroPorqueContratar');
        } else {
            $other = $request->except('_token', 'seguroCobertura');
        }
        $owner = request()->session()->get("owner");
        $carWash = request()->session()->get("carWash");
        $bank = request()->session()->get("bank");
        $owner['aceitouContatoWhats'] = isset($owner['aceitouContatoWhats']) ? 'Sim' : 'Não';
        $dataPartner = [
            'owner' => $owner,
            'carWash' => $carWash,
            'bank' => $bank,
            'other' => $other,
        ];
        Mail::send('email.partner-register', ['data' => $dataPartner], function ($m) {
            $m->from('contato@carsup.com.br', 'CarsUp');
            $m->to('contato@carsup.com.br', 'CarsUp')->subject('Cadastro Parceiro');
        });
        return redirect()->route('register.step.fifth');
    }
}
