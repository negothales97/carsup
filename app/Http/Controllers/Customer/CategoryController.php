<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://carsup-api.azurewebsites.net/v1/carwash',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $json = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($json);
        $institutes = Institute::hydrate($response);
        return view('customer.pages.institute.index')
        ->with('institutes', $institutes);
    }
}
