<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about()
    {
        return view('customer.pages.about.index');
    }
    public function policy()
    {
        return view('customer.pages.policy.index');
    }
    public function term()
    {
        return view('customer.pages.term.index');
    }
    public function contact()
    {
        return view('customer.pages.contact.index');
    }
    public function commonQuestion()
    {
        return view('customer.pages.common-question.index');
    }
}
