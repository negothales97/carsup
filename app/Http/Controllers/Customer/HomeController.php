<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Institute;

class HomeController extends Controller
{
    public function index()
    {
        // $api_url = config('services.api.url');
        // $responseCategories = requestCurl($api_url . 'categories') ?? [];

        // $categories = Category::hydrate($responseCategories)
        //     ->where('active', true);

        // $responseInstitutes = requestCurl($api_url . 'carwashes');
        // $institutes = $responseInstitutes ?
        // Institute::hydrate($responseInstitutes)
        //     ->where('active', true) :
        // (object) [];
        return view('customer.pages.home.index');
            // ->with('categories', $categories)
            // ->with('institutes', $institutes);
    }

}
