<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->is('passo-1*')) {
            return [
                'firstName' => 'required',
                'lastName' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'cpf' => 'required',
                'rg' => 'required',
                'organ' => 'required',
            ];
        }

        if (request()->is('passo-2*')) {
            return [
                "name" => 'required',
                "socialName" => 'required',
                "phone" => 'required',
                "cnpj" => 'required',
                "cep" => 'required',
                "street" => 'required',
                "number" => 'required',
                "complement" => 'nullable',
                "neighborhood" => 'required',
                "city" => 'required',
                "uf" => 'required',
            ];
        }
        if (request()->is('passo-3*')) {
            return [
                "bank" => 'required',
                "agency" => 'required',
                "account" => 'required',
                "digit" => 'required',
                "cnpj" => 'required',
                "type_count" => "required",
            ];
        }
        if (request()->is('passo-4*')) {
            $rules = [
                "password" => 'required|min:6',
                "qtdLavaPorMes" => 'required',
                "seguroPossui" => 'required',
                "productClean" => "required",
            ];
        }


        return $rules;
    }
}
