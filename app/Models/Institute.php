<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    protected $fillable = [
        'name',
        'logoURL',
        'active',
        'rating',
        'id',
        'distance',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
}
