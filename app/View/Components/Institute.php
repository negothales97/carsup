<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Institute extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $institutes;
    public function __construct($institutes)
    {
        $this->institutes = $institutes;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.institute');
    }
}
