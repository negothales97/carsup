<?php

function requestCurl($url)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));
    $json = curl_exec($curl);

    curl_close($curl);
    return json_decode($json);
}

function convertToKm($value)
{
    return convertMoneyUsaToBrazil($value /1000);
}

function convertMoneyBrazilToUsa($value)
{
    $value = str_replace(',', '.', str_replace('.', '', $value));
    $value = floatval($value);

    return $value;
}
function convertMoneyUsaToBrazil($value, $decimal = 2)
{
    $value = number_format($value, $decimal, ',', '.');

    return $value;
}