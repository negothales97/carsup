<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        @include('customer.includes.head')
    </head>

    <body>

        @include('customer.includes.header')

        @yield('content')

        @include('customer.includes.footer')
        @include('customer.includes.scripts')

        @include('customer.includes.modals')

        @yield('scripts')

    </body>

</html>
