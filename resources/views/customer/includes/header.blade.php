<header class="header">

    <div class="container">

        <div class="row">

            <div class="col-12">

                <a href="https://apps.apple.com/br/app/carsup/id1528913762"><img class="download-header" id="ios"
                        src="{{ asset('images/ios.png') }}" alt="Download IOS"></a>

                <a href="https://play.google.com/store/apps/details?id=br.com.carsup"><img class="download-header"
                        id="android" src="{{ asset('images/android.png') }}" alt="Download Android"></a>
                @if (false)
                    <div class="box-delivery">
                        <span>ENDEREÇO</span>
                        <a href="{{ route('location') }}" class="location"><img src="{{ asset('images/place.png') }}"
                                alt="Icon"></a>
                    </div>
                @endif
                <ul class="main-menu">
                    <li><a href="{{ route('register.step.first') }}" id="register">Cadastrar estabelecimento</a></li>

                    <li><a href="https://portal.carsup.com.br/" target="_blank" id="login">Entrar</a></li>
                </ul>
            </div>
        </div>

    </div>



    <button class="btn-menu-toggle" type="button">
        <div class="row-menu-toggle"></div>
        <div class="row-menu-toggle"></div>
        <div class="row-menu-toggle"></div>
    </button>

    <div id="menu-mobile">
        <div class="box-title-menu-mob">
            <strong>Menu</strong>
            <button id="btn-close-menu"><i class="fas fa-long-arrow-alt-right"></i></button>
        </div>

        <ul id="main-menu-mobile">
            <li><a href="{{ route('register.step.first') }}" id="register">Cadastrar estabelecimento</a></li>
            <li><a href="https://portal.carsup.com.br/" id="login">Entrar</a></li>
            <li><a href="https://apps.apple.com/br/app/carsup/id1528913762"><img class="download-header-mobile" id="ios"
                        src="{{ asset('images/ios.png') }}" alt="Download IOS"></a>
            </li>
            <li><a href="https://play.google.com/store/apps/details?id=br.com.carsup"><img
                        class="download-header-mobile" id="android" src="{{ asset('images/android.png') }}"
                        alt="Download Android"></a></li>

        </ul>
    </div>
</header>
