<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
    integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<!-- Input Mask e Mask money -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
<script src="{{asset('js/mask.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script type="text/javascript">

    /*$(document).scroll(function() {

        var scrollTop = $(window).scrollTop();

        if (scrollTop > 200 ) {

            $('.menu-scroll').css('display', 'block');

        }

        else{

            $('.menu-scroll').css('display', 'none');

        }

    });*/

    $(document).ready( function(){
        let locationSession = sessionStorage.getItem('location');
        let locationData = JSON.parse(locationSession);
        $('.location').append(locationData.address);
    });

    $(document).on('click', '#scroll', function(event) {
        var tela = $(window).width();
        if (tela < 768) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 20
            }, 800);
        } else {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - 180
            }, 800);
        }
    });

    $('.slider-carousel').slick({
        prevArrow: $('.prev-carousel'),
        nextArrow: $('.next-carousel'),
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2500,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 2
            }
        }]
    });

    $('.slider-testimonial').slick({
        prevArrow: $('.prev-carousel-testimonial'),
        nextArrow: $('.next-carousel-testimonial'),
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 6500,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 1
            }
        }]
    });
    $('.btn-menu-toggle').click(function() {
        $('#menu-mobile').show('slide', {
            direction: 'right'
        }, 1000);
    });

    $('#btn-close-menu').click(function() {
        $('#menu-mobile').hide('slide', {
            direction: 'right'
        }, 1000);
    });
</script>
