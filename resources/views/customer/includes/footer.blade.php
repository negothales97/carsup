<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="box-footer">
                    <h3>CarsUp</h3>
                    <ul>
                        <li><a href="{{route('page.about')}}">Quem somos</a></li>
                        <li><a href="{{route('page.policy')}}">Política de Privacidade</a></li>
                        <li><a href="{{route('page.term')}}">Termos e condições de uso</a></li>
                        <li><a href="{{route('register.step.first')}}">Cadastre seu estabelecimento</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="box-footer">
                    <h3>Ajuda</h3>
                    <ul>
                        <li><a href="{{route('page.contact')}}">Fale conosco</a></li>
                        <li><a href="{{route('page.common.question')}}">Perguntas frequentes</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="social">
                    <a href="https://facebook.com/carsupapp" target="_blank">
                        <div class="box-midia-footer" style="background:#3e5b99;">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </div>
                    </a>
                    <a href="https://www.instagram.com/carsup/" target="_blank">
                        <div class="box-midia-footer" style="background:#f2b08d;">
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </div>
                    </a>
                    <a href="https://www.youtube.com/channel/UCrVhmKSZkF_QXbjIwaY1Lrg" target="_blank">
                        <div class="box-midia-footer" style="background:#f00;">
                            <i class="fab fa-youtube" aria-hidden="true"></i>
                        </div>
                    </a>
                    <br>
                </div>
                <div class="msg-social">
                    <p>Nos sigam em nossas redes sociais</p>
                </div>
            </div>
        </div>
    </div>
    <div class="credit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>&copy; COPYRIGHT 2020 CARSUP. TODOS OS DIREITOS RESERVADOS.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
