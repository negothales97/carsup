@extends('customer.templates.default')
@section('title', 'Home')

@section('description', 'Home')

@section('content')
    <section id="banner">

        <img id="car-banner" src="images/car.png" alt="Car">

        <img id="cel-banner" src="images/cel.png" alt="Cel">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <div class="banner-first-row">

                        <img src="images/logo.png" alt="Logo">

                    </div>

                </div>

            </div>
            <div class="row">

                <div class="col-sm-12">
                    <p class="banner-second-row">Descubra estabelecimentos perto de você</p>
                    @if (false)
                        <button class="get-location" onclick="window.location.href='{{ route('location') }}';">Buscar
                            endereço <img src="{{ asset('images/location.png') }}" alt="Local"></button>
                    @endif
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="box-hashtags">
                        <div class="hashtags">
                            <p>#levaetraz</p>
                        </div>
                        <div class="hashtags">
                            <p>#lavagem</p>
                        </div>
                        <div class="hashtags">
                            <p>#higienização</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="content-home">
        <div id="particles-container"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="box-video">

                        <iframe width="560" height="315" src="https://www.youtube.com/embed/dAjn6TD2RBA" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>

                    </div>

                </div>

                <div class="col-sm-6">
                    <div class="box-video-download">
                        <h2>
                            Encontre o lava-rápido ou centro de estética mais próximo!
                        </h2>
                        <a href="https://apps.apple.com/br/app/carsup/id1528913762"><img src="images/ios.png" alt="IOS"></a>
                        <a href="https://play.google.com/store/apps/details?id=br.com.carsup"><img src="images/android.png"
                                alt="Android"></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="box-video">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/c45NtafCXBU" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-sm-6">

                    <div class="box-video-register">

                        <h2>

                            <span>O aplicativo certo para o seu negócio!</span><br>

                            Cadastre-se agora mesmo!

                        </h2>

                        <button onclick="window.location.href='{{ route('register.step.first') }}'">Cadastre-se</button>

                    </div>

                </div>

            </div>
            @if (false)
                <div class="row">

                    <div class="col-md-12">

                        <h4 class="title-section">Categorias</h4>

                    </div>

                </div>

                <div class="slider">

                    <button class="prev-carousel"><i class="fas fa-chevron-left"></i></button>

                    <button class="next-carousel"><i class="fas fa-chevron-right"></i></button>



                    <div class="slider-carousel">

                        @forelse ($categories as $category)
                            <div class="box-banner-item">

                                <a href="{{ route('institute.index', ['resourceId' => $category->id]) }}">

                                    <div class="image-background"
                                        style="background: url(images/categoria2.jpg); background-size: cover;"></div>

                                    <div class="box-name-categories">

                                        <p>{{ $category->name }}</p>

                                    </div>

                                </a>

                            </div>
                        @empty

            @endforelse




        </div>

        </div>
        @endif
        </div>

    </section>



    @if (false)
        <section id="best">

            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <h4 class="title-section">Os melhores da CarsUp</h4>

                    </div>

                </div>
                <x-institute :institutes="$institutes" />
            </div>

        </section>
    @endif



    <section id="mundo-empresarial">

        <img id="img-mundo" src="images/logo-band.png" alt="Band">

        <!--<div class="background-mundo"></div>-->

        <div class="container">

            <div class="row">

                <div class="col-sm-8 offset-sm-4">

                    <h3>Conheça um pouco mais sobre a CarsUp</h3>

                    <p>Participação no programa Mundo Empresarial, da Band</p>

                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/EUgIC2NvEnM" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>

                </div>

            </div>

        </div>

    </section>



    <section id="testimonial">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h4 class="title-section">Quem usa, recomenda</h4>

                </div>

            </div>

            <div class="slider">

                <button class="prev-carousel-testimonial"><i class="fas fa-chevron-left"></i></button>

                <button class="next-carousel-testimonial"><i class="fas fa-chevron-right"></i></button>



                <div class="slider-testimonial">

                    <div class="flex-row">

                        <div class="divider-box">

                            <div class="box-testimonial">

                                <div class="caption">

                                    <img src="images/anibal.jpg" alt="Usuário">

                                    <h4>Anibal</h4>

                                    <p class="flex-text">

                                        "Tenho imenso prazer de ter como parceiro a Carsup e ser o pioneiro na minha região,
                                        aderir a tecnologia é fundamental para estar dentro do mercado! Espero no futuro
                                        breve poder ir até aí conhecer a estrutura da Carsup e agradecer pessoalmente."

                                    </p>

                                    <div class="name-company-user">

                                        <p>Mavericks - Estética Automotiva</p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="flex-row">

                        <div class="divider-box">

                            <div class="box-testimonial">

                                <div class="caption">

                                    <img src="images/thiago.jpg" alt="Usuário">

                                    <h4>Thiago Lima</h4>

                                    <p class="flex-text">

                                        "Aplicativo sensacional, finalmente algo foi pensado para nós prestadores, uma
                                        maneira muito mais fácil de atender meus clientes. Obrigado pelo excelente
                                        atendimento para mim e para minha equipe."

                                    </p>

                                    <div class="name-company-user">

                                        <p>Ecolimp</p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="flex-row">

                        <div class="divider-box">

                            <div class="box-testimonial">

                                <div class="caption">

                                    <img src="images/karine.jpg" alt="Usuário">

                                    <h4>Karine Leite</h4>

                                    <p class="flex-text">

                                        "Quase não acreditei, que eu lavei meu carro em um sábado sem pegar filas! Agendei
                                        pelo app e fui atendida com hora marcada!! Ta ai o app que faltava na minha vida."

                                    </p>

                                    <div class="name-company-user">

                                        <p>Motorista de App</p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="flex-row">

                        <div class="divider-box">

                            <div class="box-testimonial">

                                <div class="caption">

                                    <img src="images/louise.jpg" alt="Usuário">

                                    <h4>Louise Vieira</h4>

                                    <p class="flex-text">

                                        "Fala sério, que ideia maravilhosa! Já pensou poder avaliar os lava rápidos depois
                                        de ser atendida? Agora eu só escolho os que forem 5 estrelas!"

                                    </p>

                                    <div class="name-company-user">

                                        <p>Consulmidor Moderno</p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="flex-row">

                        <div class="divider-box">

                            <div class="box-testimonial">

                                <div class="caption">

                                    <img src="images/alessandro.jpg" alt="Usuário">

                                    <h4>Alessandro Dias</h4>

                                    <p class="flex-text">

                                        "Poder escolher o entre o prestador de menor preço, menor distância, com a melhor
                                        avaliação e ainda por cima ser atendido com hora marcada?! Porque vocês não
                                        apareceram antes Carsup?"

                                    </p>

                                    <div class="name-company-user">

                                        <p>Consulmidor Moderno</p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="flex-row">

                        <div class="divider-box">

                            <div class="box-testimonial">

                                <div class="caption">

                                    <img src="images/carlos.jpg" alt="Usuário">

                                    <h4>Carlos Conte</h4>

                                    <p class="flex-text">

                                        "Com a Carsup eu agora tenho mais um canal de vendas para a minha estética
                                        automotiva, posso atender meus clientes de maneira bem mais fácil e organizada, além
                                        de encontrar novos clientes no aplicativo."

                                    </p>

                                    <div class="name-company-user">

                                        <p>Details Estúdio</p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <section id="call">

        <div class="container">

            <div class="row">

                <div class="col-sm-6 offset-sm-3">

                    <div class="box-call">

                        <img src="images/logo.png" alt="Logo">

                        <h1>Encontre o lava rápido ou centro de estética ideal mais próximo de você</h1>

                        <div class="box-download">

                            <a href="https://apps.apple.com/br/app/carsup/id1528913762"><img class="download-ios"
                                    src="images/ios.png" alt="IOS"></a>

                            <a href="https://play.google.com/store/apps/details?id=br.com.carsup"><img
                                    class="download-android" src="images/android.png" alt="Android">

                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <section id="box-home">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <h4 class="title-section" style="text-align: center;">O aplicativo certo para o seu negócio!</h4>

                </div>

            </div>

            <div class="row">

                <div class="col-12 col-sm">

                    <div class="box-content-home">

                        <img src="images/contact.png" alt="Icon">

                        <h4>Agendamentos</h4>

                        <p>

                            Utilize o sistema de agendamento online exclusivo da Carsup com Lembretes automáticos sempre que
                            receber uma solicitação.

                        </p>

                    </div>

                </div>

                <div class="col-12 col-sm">

                    <div class="box-content-home">

                        <img src="images/screen.png" alt="Icon">

                        <h4>Gestão Simplificada</h4>

                        <p>

                            Gerencie seu negócio, obtenha relatórios e tenha controle de todos os serviços atendidos pela
                            plataforma.

                        </p>

                    </div>

                </div>

                <div class="col-12 col-sm">

                    <div class="box-content-home">

                        <img src="images/financial.png" alt="Icon">

                        <h4>Controle Financeiro</h4>

                        <p>

                            Fluxo de caixa, saldo acumulado, antecipação de valores, notas fiscais e relatórios de todos
                            seus atendimentos.

                        </p>

                    </div>

                </div>

                <div class="col-12 col-sm">

                    <div class="box-content-home">

                        <img src="images/internet.png" alt="Icon">

                        <h4>Acesso Web</h4>

                        <p>

                            Acesse o portal Carsup e inclua e edite informações como fotos, serviços, dados pessoais e da
                            empresa. Tudo de maneira simples e fácil de usar.

                        </p>

                    </div>

                </div>

                <div class="col-12 col-sm">

                    <div class="box-content-home">

                        <img src="images/smartphone.png" alt="Icon">

                        <h4>Aplicativo Exclusivo</h4>

                        <p>

                            Aplicativo em duas versões, para usuários e prestadores, com agenda gerenciável, comparações de
                            preços, avaliações e muito mais.

                        </p>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <section id="monthly-payment">

        <div class="container">

            <div class="row">

                <div class="col-sm-5">

                    <div class="box-monthly">

                        <div class="box-left">

                            <h3>1º Cadastre-se</h3>

                            <p>Assim que validarmos seu cadastro, liberamos seu acesso</p>

                            <h3>2º Baixe o aplicativo</h3>

                            <p>CarsUp Parceiros</p>

                            <div class="box-download">

                                <!--<a href="#"><img class="download-ios" src="images/ios.png" alt="IOS"></a>-->

                                <a href="https://play.google.com/store/apps/details?id=br.com.carsup.partner"><img
                                        class="download-android" src="images/android.png" alt="Android">

                                </a>

                            </div>

                            <span>Sem mensalidade e sem fidelidade</span>

                        </div>

                    </div>

                </div>

                <div class="col-sm-7">

                    <h1>Venha para a <br>CarsUp!</h1>

                    <h4>Dê um Up no seu negócio!</h4>

                    <button onclick="window.location.href='{{ route('register.step.first') }}';">Realizar cadastro</button>

                </div>

            </div>

        </div>

    </section>
@endsection
@section('scripts')
<script type="text/javascript">
    particlesJS.load('particles-container', `{{asset('plugins/particlesjs/particlesjs-config.json')}}`);
</script>

@endsection