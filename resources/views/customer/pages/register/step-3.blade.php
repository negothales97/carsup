@extends('customer.templates.default')
@section('title', 'Dados - Bancários')

@section('description', 'Dados - Bancários')

@section('content')
    <section class="steps">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-hiring-steps">
                        <h2>Etapas do cadastro</h2>
                        <ul>
                            <li>
                                <div class="footsteps footsteps-completed">1</div>
                                <h4 class="step-completed">Dados - Proprietário</h4>
                            </li>
                            <li>
                                <div class="footsteps footsteps-completed">2</div>
                                <h4 class="step-completed">Dados - Lava Rápido</h4>
                            </li>
                            <li>
                                <div class="footsteps footsteps-active">3</div>
                                <h4 class="step-active">Dados Bancários</h4>
                            </li>
                            <li>
                                <div class="footsteps">4</div>
                                <h4>Finalizar Cadastro</h4>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="box-content-pages">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ route('register.form.third') }}" method="post">
                                    @csrf
                                    <fieldset class="holder-data">
                                        <legend>Dados Bancários</legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="bank">Banco</label>
                                                    <select class="bank" name="bank" style="width: 100%;">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="agency" label="Agência (sem dígito)"
                                                    placeholder="Ex: 0097" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="account" label="Conta" placeholder="Ex: 19780" />
                                            </div>
                                            <div class="col-sm-6">
                                                <x-input name="digit" label="Dígito" placeholder="Ex: 8" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="cnpj" clazz="input-cnpj" label="CNPJ"
                                                    placeholder="Ex: 12.345.678/0000-12" />
                                            </div>
                                        </div>
                                        <div class="box-radios">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label>Tipo de Conta</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label class="lbl-caracteristicas">
                                                            <input type="radio" class="form-control" value="corrente"
                                                                name="type_count" checked>
                                                            <span class="checkmark"></span>
                                                            Conta Corrente
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label class="lbl-caracteristicas"><input type="radio"
                                                                class="form-control" value="poupanca"
                                                                name="type_count"><span class="checkmark"></span>Conta
                                                            Poupança</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <button class="form-control btn-form">Continuar</button>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <a class="back-register" href="#">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            axios.get(`{{ route('api.bank.index') }}`)
                .then(response => {
                    let banks = response.data;
                    let data = [];
                    banks.forEach((bank, item) => {
                        data.push({
                            id: bank.Code,
                            text: `${bank.Code} - ${bank.Name}`,
                        });
                    });
                    console.log(data);
                    $('.bank').select2({
                        data: data
                    });
                })
                .catch(response => {
                    console.log(response);
                });

        });

    </script>
@endsection
