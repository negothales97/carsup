@extends('customer.templates.default')
@section('title', 'Dados - Finalizar Cadastro')

@section('description', 'Dados - Finalizar Cadastro')

@section('content')

<section class="steps">

	<div class="container">

		<div class="row">

			<div class="col-md-4">

				<div class="box-hiring-steps">

					<h2>Etapas do cadastro</h2>

					<ul>

						<li><div class="footsteps footsteps-completed">1</div><h4 class="step-completed">Dados - Proprietário</h4></li>

						<li><div class="footsteps footsteps-completed">2</div><h4 class="step-completed">Dados - Lava Rápido</h4></li>

						<li><div class="footsteps footsteps-completed">3</div><h4 class="step-completed">Dados Bancários</h4></li>

						<li><div class="footsteps footsteps-completed">4</div><h4 class="step-completed">Finalizar Cadastro</h4></li>

					</ul>

				</div>

			</div>

			<div class="col-md-8">

				<div class="box-content-pages">

					<div class="row">

						<div class="col-md-12">

							<img src="images/check.jpg" alt="Check">

							<p id="register-completed">Estabelecimento cadastrado com<br>

								<span>Sucesso</span>

							</p>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</section>
@endsection