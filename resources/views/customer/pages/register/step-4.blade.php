@extends('customer.templates.default')
@section('title', 'Dados - Finalizar Cadastro')

@section('description', 'Dados - Finalizar Cadastro')

@section('content')
    <section class="steps">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <div class="box-hiring-steps">

                        <h2>Etapas do cadastro</h2>

                        <ul>

                            <li>
                                <div class="footsteps footsteps-completed">1</div>
                                <h4 class="step-completed">Dados - Proprietário</h4>
                            </li>

                            <li>
                                <div class="footsteps footsteps-completed">2</div>
                                <h4 class="step-completed">Dados - Lava Rápido</h4>
                            </li>

                            <li>
                                <div class="footsteps footsteps-completed">3</div>
                                <h4 class="step-completed">Dados Bancários</h4>
                            </li>

                            <li>
                                <div class="footsteps footsteps-active">4</div>
                                <h4 class="step-active">Finalizar Cadastro</h4>
                            </li>

                        </ul>

                    </div>

                </div>

                <div class="col-md-8">

                    <div class="box-content-pages">

                        <div class="row">

                            <div class="col-md-12">

                                <form action="{{ route('register.form.fourth') }}" method="post">
                                    @csrf
                                    <fieldset class="holder-data">

                                        <legend>Finalizar Cadastro</legend>

                                        <div class="row">

                                            <div class="col-sm-12">
                                                <x-input name="password" label="Senha de login no painel" placeholder="Ex: •••••••" type="password" />
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <x-input name="qtdLavaPorMes" label="Lavagens por mês" placeholder="Ex: 1200" />
                                            </div>

                                        </div>

                                        <div class="box-radios">

                                            <div class="row">

                                                <div class="col-sm-12">

                                                    <label>Possui seguro?</label>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label class="lbl-caracteristicas"><input type="radio"
                                                                class="form-control seguroPossui" value="true" name="seguroPossui"
                                                                ><span class="checkmark"></span>Sim</label>
                                                    </div>
                                                </div>

                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <label class="lbl-caracteristicas"><input type="radio"
                                                                class="form-control seguroPossui" value="false"
                                                                name="seguroPossui"><span
                                                                class="checkmark"></span>Não</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="whySeguro" style="display:none;">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Pretende contratar seguro:</label>
                                                            <div class="row">
                                                                <div class="col-sm-5">
                                                                    <div class="form-group">
                                                                        <label class="lbl-caracteristicas"><input
                                                                                type="radio" class="form-control"
                                                                                name="seguroPretendeContratar"
                                                                                id="trueSeguroPretendeContratar"
                                                                                value="true"><span
                                                                                class="checkmark"></span>Sim</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <div class="form-group">
                                                                        <label class="lbl-caracteristicas"><input
                                                                                type="radio" class="form-control"
                                                                                name="seguroPretendeContratar"
                                                                                id="falseSeguroPretendeContratar"
                                                                                value="false"><span
                                                                                class="checkmark {{ $errors->has('falseSeguroPretendeContratar') ? 'is-invalid' : '' }}"></span>Não</label>
                                                                    </div>
                                                                    @if ($errors->has('firstName'))
                                                                        <span class="help-block">
                                                                            <small>{{ $errors->first('falseSeguroPretendeContratar') }}</small>
                                                                        </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="seguroPorqueContratar">Por que?</label>
                                                            <input type="text" name="seguroPorqueContratar"
                                                                id="seguroPorqueContratar" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cover" style="display:none;">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="seguroCobertura">Valor de cobertura</label>
                                                            <input type="text" name="seguroCobertura" id="seguroCobertura"
                                                                class="form-control input-money">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="productClean">Fornecedor de produtos de limpeza</label>
                                                    <input type="text" id="productClean" class="form-control"
                                                        name="productClean" placeholder="Ex: Clean Master Ltda.">
                                                </div>

                                            </div>

                                        </div>

                                    </fieldset>

                                    <div class="row">

                                        <div class="col-sm-4">

                                            <div class="form-group">

                                                <button class="form-control btn-form">Cadastrar</button>

                                            </div>

                                        </div>

                                        <div class="col-sm-4">

                                            <div class="form-group">

                                                <a class="back-register" href="#">Voltar</a>

                                            </div>

                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection

@section('scripts')
    <script type="text/javascript">
        $('.seguroPossui').on('change', function() {
            let seguroPossui = $(this).val();
            seguroPossuiValidate(seguroPossui);

        });
        $(document).ready(function() {
            let seguroPossui = $(this).val();
            seguroPossuiValidate(seguroPossui);
        });

        function seguroPossuiValidate(seguroPossui) {
            if (seguroPossui == 'false') {
                $('.whySeguro').slideDown();
                $('.cover').slideUp();
            } else if (seguroPossui == 'true') {
                $('.whySeguro').slideUp();
                $('.cover').slideDown();
            }
        }

    </script>
@endsection
