@extends('customer.templates.default')
@section('title', 'Dados - Lava Rápido')

@section('description', 'Dados - Lava Rápido')

@section('content')
    <section class="steps">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <div class="box-hiring-steps">

                        <h2>Etapas do cadastro</h2>

                        <ul>

                            <li>
                                <div class="footsteps footsteps-completed">1</div>
                                <h4 class="step-completed">Dados - Proprietário</h4>
                            </li>

                            <li>
                                <div class="footsteps footsteps-active">2</div>
                                <h4 class="step-active">Dados - Lava Rápido</h4>
                            </li>

                            <li>
                                <div class="footsteps">3</div>
                                <h4>Dados Bancários</h4>
                            </li>

                            <li>
                                <div class="footsteps">4</div>
                                <h4>Finalizar Cadastro</h4>
                            </li>

                        </ul>

                    </div>

                </div>

                <div class="col-md-8">

                    <div class="box-content-pages">

                        <div class="row">

                            <div class="col-md-12">

                                <form action="{{ route('register.form.second') }}" method="post">
                                    @csrf
                                    <fieldset class="holder-data">

                                        <legend id="car-wash">Dados do Lava Rápido</legend>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <x-input name="name" label="Nome" placeholder="Ex: Lava Rápido Total" />
                                            </div>

                                            <div class="col-sm-6">
                                                <x-input name="socialName" label="Razão Social"
                                                    placeholder="Ex: Lava Rápido Total Ltda." />
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <x-input name="phone" clazz="input-phone" label="Telefone" placeholder="Ex: (11) 98030-4060" />
                                            </div>

                                            <div class="col-sm-6">
                                                <x-input name="cnpj" clazz="input-cnpj" label="CNPJ" placeholder="Ex: 12.345.678/0000-12" />
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="cep" clazz="input-cep" label="CEP" placeholder="Ex: 12345-678" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <x-input name="street" label="Logradouro"
                                                    placeholder="Ex: Rua Alameda Zenith" />
                                            </div>
                                            <div class="col-sm-3">
                                                <x-input name="number" label="Número" placeholder="Ex: 879" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="complement" label="Complemento" placeholder="Ex: Fundos" />
                                            </div>
                                            <div class="col-sm-6">
                                                <x-input name="neighborhood" label="Bairro"
                                                    placeholder="Ex: Jardim das Flores" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="city" label="Cidade" placeholder="Ex: São Paulo" />
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="uf">Estado</label>
                                                    <select name="uf"
                                                        class="form-control {{ $errors->has('uf') ? 'is-invalid' : '' }}"
                                                        id="uf">
                                                        <option selected disabled hidden>Selecione..</option>
                                                        <option {{ old('uf') == 'AC' ? 'selected' : '' }} value="AC">AC
                                                        </option>
                                                        <option {{ old('uf') == 'AL' ? 'selected' : '' }} value="AL">AL
                                                        </option>
                                                        <option {{ old('uf') == 'AP' ? 'selected' : '' }} value="AP">AP
                                                        </option>
                                                        <option {{ old('uf') == 'AM' ? 'selected' : '' }} value="AM">AM
                                                        </option>
                                                        <option {{ old('uf') == 'BA' ? 'selected' : '' }} value="BA">BA
                                                        </option>
                                                        <option {{ old('uf') == 'CE' ? 'selected' : '' }} value="CE">CE
                                                        </option>
                                                        <option {{ old('uf') == 'DF' ? 'selected' : '' }} value="DF">DF
                                                        </option>
                                                        <option {{ old('uf') == 'ES' ? 'selected' : '' }} value="ES">ES
                                                        </option>
                                                        <option {{ old('uf') == 'GO' ? 'selected' : '' }} value="GO">GO
                                                        </option>
                                                        <option {{ old('uf') == 'MA' ? 'selected' : '' }} value="MA">MA
                                                        </option>
                                                        <option {{ old('uf') == 'MT' ? 'selected' : '' }} value="MT">MT
                                                        </option>
                                                        <option {{ old('uf') == 'MS' ? 'selected' : '' }} value="MS">MS
                                                        </option>
                                                        <option {{ old('uf') == 'MG' ? 'selected' : '' }} value="MG">MG
                                                        </option>
                                                        <option {{ old('uf') == 'PA' ? 'selected' : '' }} value="PA">PA
                                                        </option>
                                                        <option {{ old('uf') == 'PB' ? 'selected' : '' }} value="PB">PB
                                                        </option>
                                                        <option {{ old('uf') == 'PR' ? 'selected' : '' }} value="PR">PR
                                                        </option>
                                                        <option {{ old('uf') == 'PE' ? 'selected' : '' }} value="PE">PE
                                                        </option>
                                                        <option {{ old('uf') == 'PI' ? 'selected' : '' }} value="PI">PI
                                                        </option>
                                                        <option {{ old('uf') == 'RJ' ? 'selected' : '' }} value="RJ">RJ
                                                        </option>
                                                        <option {{ old('uf') == 'RN' ? 'selected' : '' }} value="RN">RN
                                                        </option>
                                                        <option {{ old('uf') == 'RS' ? 'selected' : '' }} value="RS">RS
                                                        </option>
                                                        <option {{ old('uf') == 'RO' ? 'selected' : '' }} value="RO">RO
                                                        </option>
                                                        <option {{ old('uf') == 'RR' ? 'selected' : '' }} value="RR">RR
                                                        </option>
                                                        <option {{ old('uf') == 'SC' ? 'selected' : '' }} value="SC">SC
                                                        </option>
                                                        <option {{ old('uf') == 'SP' ? 'selected' : '' }} value="SP">SP
                                                        </option>
                                                        <option {{ old('uf') == 'SE' ? 'selected' : '' }} value="SE">SE
                                                        </option>
                                                        <option {{ old('uf') == 'TO' ? 'selected' : '' }} value="TO">TO
                                                        </option>
                                                    </select>
                                                    @if ($errors->has('uf'))
                                                        <span class="help-block">
                                                            <small>{{ $errors->first('uf') }}</small>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <button class="form-control btn-form">Continuar</button>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <a class="back-register" href="#">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('scripts')
    <script type="text/javascript">
    $(document).ready(function() {
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if (validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#street").val("...");
                        $("#neighborhood").val("...");
                        $("#city").val("...");
                        $("#uf").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#street").val(dados.logradouro);
                                $("#neighborhood").val(dados.bairro);
                                $("#city").val(dados.localidade);
                                $("#uf").val(dados.uf);

                                $("#street").attr("readonly", true);
                                $("#neighborhood").attr("readonly", true);
                                $("#city").attr("readonly", true);
                                $("#uf").attr("readonly", true);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });
    </script>
@endsection