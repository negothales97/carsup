@extends('customer.templates.default')
@section('title', 'Dados - Proprietário')

@section('description', 'Dados - Proprietário')

@section('content')

    <section class="steps">

        <div class="container">

            <div class="row">

                <div class="col-md-4">

                    <div class="box-hiring-steps">

                        <h2>Etapas do cadastro</h2>

                        <ul>

                            <li>
                                <div class="footsteps footsteps-active">1</div>
                                <h4 class="step-active">Dados - Proprietário</h4>
                            </li>

                            <li>
                                <div class="footsteps">2</div>
                                <h4>Dados - Lava Rápido</h4>
                            </li>

                            <li>
                                <div class="footsteps">3</div>
                                <h4>Dados Bancários</h4>
                            </li>

                            <li>
                                <div class="footsteps">4</div>
                                <h4>Finalizar Cadastro</h4>
                            </li>

                        </ul>

                    </div>

                </div>

                <div class="col-md-8">

                    <div class="box-content-pages">

                        <div class="row">

                            <div class="col-md-12">

                                <form action="{{ route('register.form.first') }}" method="post">
                                    @csrf
                                    <fieldset class="holder-data">

                                        <legend>Dados do titular</legend>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="firstName" label="Nome" placeholder="Ex: João" />
                                            </div>

                                            <div class="col-sm-6">
                                                <x-input name="lastName" label="Sobrenome" placeholder="Ex: Santos Lima" />
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-sm-6">
                                                <x-input name="email" type="email" label="E-mail" placeholder="Ex: joaolima@gmail.com" />
                                            </div>

                                            <div class="col-sm-6">
                                                <x-input name="phone" clazz="input-phone" label="Telefone" placeholder="Ex: (11) 98030-4060" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="rg" label="RG" placeholder="Ex: 12.345.678-9" />
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <x-input name="organ" label="Orgão Emissor" placeholder="Ex: SSP" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <x-input name="cpf" clazz="input-cpf" label="CPF" placeholder="Ex: 123.456.789-10" />
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="box-radios">

                                        <div class="row">

                                            <div class="col-sm-12">

                                                <div class="form-group">

                                                    <label class="lbl-caracteristicas">
                                                        <input type="checkbox" checked id="aceitouContatoWhats"
                                                            class="form-control" name="aceitouContatoWhats">
                                                        <span class="checkmark checkfirst"></span>
                                                        Concordo em receber contato via Whatsapp para fins de
                                                        cadastro
                                                    </label>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">

                                            <div class="form-group">

                                                <button class="form-control btn-form">Continuar</button>

                                            </div>

                                        </div>

                                        <div class="col-sm-4">

                                            <div class="form-group">

                                            <a class="back-register" href="{{url('/')}}">Voltar</a>

                                            </div>

                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>
@endsection
