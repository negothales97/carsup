<!DOCTYPE html>

<html>

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>CarsUp</title>

    <!-- Tell the browser to be responsive to screen width -->

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">



    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">

    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">

    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">

    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">

    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">

    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">

    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">

    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">

    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">

    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">

    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">

    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">

    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">

    <link rel="manifest" href="images/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#F58634">

    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">

    <meta name="theme-color" content="#F58634">



    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">



    <link href="{{ asset('plugins/fontawesome/css/all.min.css') }}" rel="stylesheet">



    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />



    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,900i&display=swap" rel="stylesheet">



    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}?v=2.0.2">

    <link href="{{ asset('js/app.js') }}?v=1.0.0">



</head>

<body>



    <section class="content-pages company">

        <div class="container">

            <div class="row">

                <div class="col-sm-6 offset-sm-3">

                    <div class="box-location">

                        <div class="row">

                            <div class="col-sm-12">

                                <a href="{{ url('/') }}">Retornar</a>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-12">

                                <h3>Informe sua localização</h3>

                                <p>Encontre estabelecimentos próximos de você</p>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-12">

                                <form>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" id="search_input" class="form-control"
                                                name="search_input" placeholder="Buscar bairro, rua, etc">
                                        </div>
                                        <input type="hidden" id="loc_lat" />
                                        <input type="hidden" id="loc_long" />
                                        <div class="form-group">
                                            <button type="button" onclick="getLocation();">
                                                <img src="images/location.png" alt="Icon">
                                                <span>Usar minha
                                                    localização</span>
                                            </button>

                                        </div>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <footer>



    </footer>



    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
        integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js"></script>

    <!-- GoogleMaps API -->
    <script
        src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyDXruFBpALszeLY69jhYiZGjXsnszWSyf0&language=pt-BR">
    </script>
    <script type="text/javascript">
        var searchInput = 'search_input';

        $(document).ready(function() {
            var autocomplete;
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ['geocode'],
            });

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var near_place = autocomplete.getPlace();
                geoLocation = {
                    latitude: near_place.geometry.location.lat(),
                    longitude: near_place.geometry.location.lng(),
                    address: near_place.formatted_address,
                }
                setSession(geoLocation);
            });
        });

    </script>
    <!--./ GoogleMaps API -->
    <script>
        $('.establishment-search').on('click', function(e) {
            e.preventDefault();
            verifySessionScript();
        });
        $('.more-details').on('click', function(e) {
            e.preventDefault();
            url = $(this).attr('href');
            verifySessionScript(url);
        });

        function verifySessionScript(url) {
            $.ajax({
                type: "GET",
                url: "{{ route('api.session') }}",
                data: {},
                beforeSend: function() {},
                success: function(data) {},
                error: function(error) {
                    console.log(error);
                }
            });
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPartners);
            } else {
                alert("O seu navegador não suporta Geolocalização.")
            }
        }

        function showPartners(position) {
            var location = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
            };
            setSession(location);
        }

        function setSession(location) {
            $.ajax({
                type: "GET",
                url: "{{ route('api.session.set') }}",
                data: {
                    location: location
                },
                beforeSend: function() {},
                success: function(data, status) {
                    if (status == "success") {
                        sessionStorage.setItem('location', JSON.stringify(data));
                        window.location.href = "{{ route('institute.index') }}";
                    }
                },
                error: function(error) {
                    console.log(error.responseText);
                }
            });
        }

    </script>

</body>

</html>
