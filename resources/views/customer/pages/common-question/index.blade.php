@extends('customer.templates.default')
@section('title', 'Perguntas Frequentes')

@section('description', 'Perguntas Frequentes')

@section('content')
    <section class="content-pages company">

        <div class="container">

            <div class="row">

                <div class="col-sm-12">

                    <h3 class="title-section-pages">Perguntas Frequentes</h3>

                </div>

            </div>

            <div class="row">

                <div class="col-sm-9">

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="box-questions">

                                <h3>

                                    Como podemos ajudar você?<br>

                                    Quem é responsável pelo serviço prestado no veículo?

                                </h3>



                                <p>

                                    O lava rápido é o único responsável pelos serviços prestados por ele e por seus

                                    funcionários, incluindo defeitos, avarias, desconformidade com o que foi solicitado,

                                    utilização de produtos diferentes, danos, entre outros.

                                    A CarsUp não é um lava rápido, não fiscaliza o que o lava rápido faz e não participa de

                                    nenhuma etapa da prestação do serviço, apenas possibilita o encontro entre vocês.

                                    Caso tenha problemas, procure diretamente o lava rápido que o atendeu!

                                    Facilitamos a comunicação entre as partes, disponibilizando o contato telefônico do

                                    estabelecimento no qual você utilizou.

                                </p>





                                <h3>Não consigo pagar on-line</h3>

                                <p>

                                    O primeiro passo para conseguir pagar on-line é verificar se os dados do cartão estão

                                    cadastrados corretamente no aplicativo ou no site. Se estiver tudo certo, pedimos que
                                    você

                                    entre em contato com a emissora do cartão para entender se existe algum problema. Caso

                                    esteja tudo correto e, ainda assim, você não conseguir pagar online, pedimos que entre
                                    em

                                    contato com a gente.

                                </p>



                                <h3>Tive uma cobrança indevida</h3>

                                <p>

                                    Para solicitar estorno de uma cobrança indevida, pedimos que envie uma mensagem para
                                    gente

                                    contendo os dados do pedido e, principalmente, o comprovante de cobrança. Assim com
                                    essas

                                    informações em mãos, poderemos entender seu caso e providenciar o estorno do valor
                                    cobrado

                                    indevidamente.

                                </p>



                                <h3>Não recebi meu estorno</h3>

                                <p>

                                    Caso você não tenha recebido o estorno solicitado por conta de uma cobrança indevida,

                                    pedimos que envie uma mensagem para gente contendo os dados do pedido e, principalmente,
                                    o

                                    comprovante de cobrança. Assim com essas informações em mãos, poderemos entender seu
                                    caso e

                                    providenciar o estorno do valor cobrado indevidamente.

                                </p>





                                <h3>Não consigo usar o cupom de descontos</h3>

                                <p>

                                    Existem algumas regras importantes para aplicar o cupom de descontos aos serviços

                                    contratados por meio da plataforma. A primeira delas é que o cupom de descontos é válido

                                    apenas para contratações com pagamento online, ou seja, qualquer uma das opções listadas
                                    em

                                    “Pague pelo App”. A segunda regra tem a ver com o valor mínimo de serviço de cada lava

                                    rápido.Você deve verificar se o serviço que você contratou está acima do valor
                                    determinado

                                    pelo lava rápido para conseguir aplicar o cupom de descontos. Por fim, você deve checar

                                    também a validade do cupom de descontos. Para isso, basta consultar o regulamento da

                                    promoção.

                                    <br><br>



                                    Se estiver tudo certo e, ainda assim, você não conseguir aplicar o cupom de desconto a
                                    sua

                                    contratação, pedimos que entre em contato com a gente.

                                </p>





                                <h3>Não consigo excluir os dados do meu cartão</h3>



                                <p>

                                    A CarsUp armazenas os dados do seu cartão com segurança apenas no seu aparelho, e nunca
                                    em

                                    um serviço externo. É por esse motivo que você encontra o seu cartão salvo quando
                                    realiza um

                                    pedido com pagamento on-line.

                                    Caso tenha realizado seu pedido pelo site, assim que deslogar, todas as suas informações

                                    serão expiradas. Mas se você fez seu pedido pelo aplicativo, só poderá excluir os dados
                                    do

                                    seu cartão quando fizer um novo pedido e selecionar a forma de pagamento desejada. Você
                                    vai

                                    ver a opção “Trocar” ao lado do seu cartão, já pré-selecionado para o seu pagamento.
                                    Clique

                                    em “Trocar” e, depois, no menu ao lado do cartão que você quer apagar. Por fim, clique
                                    na

                                    lixeira para excluir o cartão.

                                </p>



                                <h3>Quero alterar os dados da minha conta</h3>

                                <p>

                                    Você pode alterar nome, CPF, celular e senha no aplicativo ou no site.

                                    Já o seu e-mail é a chave principal de seu cadastro, ou seja, funciona como uma

                                    identificação dentro da plataforma e não conseguimos alterá-lo. Para alterar seus dados
                                    no

                                    App, clique no seu avatar ou foto, no canto superior direito da tela. Depois basta
                                    clicar em

                                    “configurações” e então em “editar dados”. Você também pode usar a área “meu perfil” do

                                    site.

                                </p>



                                <h3>Minha conta foi invadida por alguém</h3>

                                <p>

                                    O acesso indevido a sua conta pode ter ocorrido pelo comprometimento da sua senha. Se a

                                    mesma senha é usada em mais de um serviço, o incidente de segurança em um dessesdos
                                    serviços

                                    pode comprometer os outros. Se isso aconteceu, saiba que o invasor não teve acesso aos
                                    seus

                                    dados de pagamento na CarsUp , pois eles ficam salvos apenas no seu dispositivo
                                    (Celular,

                                    Tablet ou Computador). Nesse caso, recomendamos a alteração da sua senha para uma nova
                                    mais

                                    forte, preferencialmente usando letras maiúsculas e caracteres especiais (Por exemplo @,
                                    #,

                                    !, etc).

                                </p>



                                <h3>Não encontro o meu endereço</h3>

                                <p>

                                    Se o seu CEP está errado ou se você teve qualquer outro problema para encontrar a sua

                                    localização no app ou no site, pedimos que envie uma mensagem pra gente com o seu
                                    endereço

                                    correto e completo. Assim, poderemos trabalhar para melhorar nossa precisão de
                                    localização.

                                </p>



                                <h3>Quero excluir uma avaliação que eu fiz</h3>

                                <p>

                                    Entendemos que as vezes podemos mudar de ideia! Para que nosso time te ajude a excluir
                                    uma

                                    avaliação, envie uma mensagem para CarsUp.

                                </p>



                                <h3>Quero excluir a minha conta</h3>

                                <p>

                                    Sentimos muito pela escolha de deixar a CarsUp. Para desativarmos a sua conta, envie uma

                                    mensagem para CarsUp. Se você quiser voltar a procurar e contratar lava jatos por meio
                                    da

                                    nossa plataforma, é necessário que você refaça o seu cadastro com um novo
                                    e-mail.<br><br>



                                    Quando você exclui sua conta, nós tambémexcluímos os seus dados pessoais da nossa

                                    plataforma, exceto aqueles que precisamos manter por obrigação imposta por lei (como os

                                    registros de acesso) ou para eventualmente nos defender. Mas, nesses casos, podemos
                                    acessar

                                    esses dados apenas para essas finalidades e não para reativar contas antigas.

                                </p>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection
