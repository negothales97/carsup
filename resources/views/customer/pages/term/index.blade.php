@extends('customer.templates.default')
@section('title', 'Termos de Uso')

@section('description', 'Termos de Uso')

@section('content')
<section class="content-pages company">

    <div class="container">

        <div class="row">

            <div class="col-sm-8">

                <div class="row">

                    <div class="col-sm-12">

                        <div class="box-text">

                            <h3>Para você entender melhor como funciona esta plataforma</h3>

                            <p>

                                Fizemos o máximo para reduzir e simplificar as regras da nossa Plataforma. Por isso,

                                separamos abaixo os pontos mais importantes para você, que também podem ser lidos de

                                forma

                                bem completa e detalhada nos <b> Termos de Uso a seguir.</b>

                            </p>

                            <p>

                                Além disso, estamos sempre disponíveis para tirar qualquer dúvida que você tenha pelo

                                e-mail

                                contato@carsup.com.br, chat on-line da plataforma e telefones (11) 996084086 e (11)

                                933226552.

                            </p>

                            <p>

                                <b>1) Nosso negócio é tecnologia e não prestamos nenhum tipo de serviço para

                                    veículos.</b> A <b>CARSUP</b>

                                é apenas uma plataforma intermediadora, que possibilita o encontro de empresas que

                                prestam

                                serviços, como limpeza e estética automotiva, troca de filtros, aplicação de insulfilme,

                                troca de óleo e busca e entrega do veículo com pessoas interessadas.

                            </p>



                            <p>

                                <b>2)</b> Dessa maneira, não possuímos nenhuma responsabilidade pelos serviços

                                realizados

                                nos veículos e nem pelas condutas das empresas aqui cadastradas, apenas pelo bom

                                funcionamento da plataforma. <b>Caso você tenha qualquer tipo de problema com o serviço

                                    prestado, procure diretamente quem o executou, ok?</b>

                            </p>



                            <p>

                                <b>3)</b> Ninguém paga para se cadastrar na plataforma, mas o Usuário Cliente deverá

                                pagar pelos

                                serviços do Usuário Prestador de Serviço, acrescidos de uma taxa de serviço de 20%

                                (vinte

                                por cento) da CARSUP.

                            </p>



                            <p>

                                <b>4)</b> Respeitamos todos os seus direitos, em especial o direito de arrependimento

                                dos

                                Usuários

                                Clientes no prazo de 7 (sete) dias contados após a contratação do serviço na plataforma,

                                nos

                                termos do Código de Defesa do Consumidor. Assim, caso o direito de arrependimento seja

                                exercido, não será devida nenhuma quantia ao Usuário Prestador de Serviço.

                            </p>



                            <p>

                                <b>5)</b> No caso de cancelamento pelo Usuário Cliente após o prazo de 7 dias, será

                                cobrada uma

                                multa de 25% (vinte e cinco por cento) sobre o valor total pago.

                            </p>



                            <p>

                                <b>6)</b> Caso o Usuário Prestador de Serviço cancele o serviço, o Usuário Cliente será

                                reembolsado

                                por todos os valores pagos e o Usuário Prestador de Serviço deverá pagar as taxas da

                                operadora.

                            </p>



                            <p>

                                <b>7)</b> Nos esforçamos bastante para que a nossa plataforma seja segura, mas ainda

                                assim

                                recomendamos que, antes de baixar qualquer conteúdo, você instale antivírus e programas

                                de

                                proteção.

                            </p>



                            <p>

                                <b>8)</b> Nossos Termos de Uso poderão mudar, mas você sempre poderá acessar a versão

                                mais

                                atualizada na nossa plataforma. Além disso, se formos realizar alguma ação que a lei

                                exija

                                sua autorização, você receberá um aviso antes para poder aceitar ou recusar.

                            </p>



                            <p>

                                <b>9)</b> Temos uma Política de Privacidade que trata sobre o que fazemos com os seus

                                dados

                                pessoais. É muito importante que você leia e entenda esse documento também!

                            </p>



                            <p>

                                <b>10)</b> Os Termos de Uso a seguir estão divididos da seguinte forma para facilitar o

                                seu

                                acesso

                                à informação:

                            </p>

                            <ol type="a">



                                <li> Data de Disponibilização do Texto;</li>

                                <li> Explicação dos Termos Técnicos ou em Língua Estrangeira;</li>

                                <li> Serviços;</li>

                                <li> Cadastro dos Usuários;</li>

                                <li> Formas e Condições de Pagamento e Cancelamento;</li>

                                <li> Chat Online;</li>

                                <li> Responsabilidades das Partes;</li>

                                <li> Isenção de Responsabilidade da CARSUP;</li>

                                <li> Regras de Conduta e Proibições;</li>

                                <li> Propriedade Intelectual;</li>

                                <li> Tratamento de Dados Pessoais, Privacidade e Segurança;</li>

                                <li> Desdobramentos do acesso à Plataforma;</li>

                                <li> Alterações dos Termos e Condições de Uso;</li>

                                <li> Prazo e Duração do documento;</li>

                                <li> Definição da Lei Aplicável e do Foro de eleição;</li>

                                <li> Canal de Contato;</li>

                            </ol>



                            <p>

                                CARSUP.

                            </p>

                             

                            <h3>TERMOS E CONDIÇÕES DE USO</h3>



                            <p>

                                Esta plataforma, cujo nome é <b>CARSUP</b>, é de propriedade, mantida e operada por

                                ROSANA

                                GONCALVES CAPELLA 02161038869, pessoa jurídica de direito privado, inscrita no CNPJ sob

                                o nº

                                34.199.420/0001-60, com sede na Avenida Antônio Ricardo da Silva, 77, Jardim Nossa

                                Senhora

                                do Carmo, CEP 08.270-560, na Cidade de São Paulo, Estado de São Paulo, Brasil.

                            </p>





                            <p>

                                Este documento visa prestar informações sobre o modo de utilização da Plataforma e suas

                                ferramentas, condições, nomenclaturas, direitos e deveres, além de preservar a

                                privacidade

                                dos Usuários. Alertamos que todo o texto deve ser lido com atenção e, caso você não

                                concorde

                                com o conteúdo de nossos termos e/ou política de privacidade, não dê prosseguimento a

                                navegação ou a utilização de nossos serviços. Recomendamos, ainda, que caso seja aceito,

                                que

                                você armazene ou imprima uma cópia deste contrato, incluindo todas as políticas.

                            </p>



                            <p>

                                <b> 1. DATA DE DISPONIBILIZAÇÃO DO TEXTO</b>

                            </p>

                            <p>

                                O presente documento foi redigido e disponibilizado em 29/07/2019.

                            </p>



                            <p>

                                <b>2. EXPLICAÇÃO DOS TERMOS TÉCNICOS OU EM LÍNGUA ESTRANGEIRA</b>

                            </p>

                            <p>

                                Abaixo estão dispostos os significados das nomenclaturas técnicas e termos na língua

                                inglesa.

                                Login: É o processo que permite o acesso a um sistema informático, controlado por meio

                                de

                                identificação e autenticação do Usuário pelas credenciais fornecidas por esse mesmo

                                internauta.

                                Online: Termo da língua inglesa cujo significado literal é “em linha”. É habitualmente

                                usado

                                para designar que um determinado Usuário da internet ou de outra rede de computadores

                                está

                                conectado à rede.

                                Usuário Cliente: Pessoas físicas, externas ao sistema, que usufruem do serviço prestado

                                pela

                                plataforma, ao buscar empresas que prestam serviços para veículos.

                                Usuário Prestador de Serviço: Pessoas jurídicas, independentes e externas ao sistema,

                                que

                                oferecem serviços para veículos, como limpeza e estética automotiva, troca de filtros,

                                aplicação de insulfilme, troca de óleo e busca e entrega do veículo.

                                Usuários: Denominação em conjunto dos Usuários Clientes e Usuários Prestadores de

                                Serviços.

                            </p>

                            <p>

                                <b> 3. SERVIÇOS</b>

                            </p>

                            <p>

                                3.1. A <b>CARSUP</b> consiste, mas não se limita, a uma plataforma de tecnologia que

                                possibilita o

                                encontro de Usuários Clientes com Usuários Prestadores de Serviços, que fornecem

                                serviços

                                como: limpeza e estética automotiva, troca de filtros, aplicação de insulfilme e troca

                                de

                                óleo, com a opção de atendimento a domicílio ou de levar e trazer o veículo.

                            </p>



                            <p>

                                <b>3.2. A CARSUP não presta serviços para veículos, não possui responsabilidade pelos

                                    serviços

                                    realizados nos veículos e nem pelas condutas das empresas aqui cadastradas, apenas

                                    pelo bom

                                    funcionamento da plataforma.</b>

                            </p>



                            <p>

                                <b>3.3 Em caso de danos, avarias, prejuízos, falhas na execução do serviço, perdas ou

                                    qualquer

                                    tipo de problema, procure diretamente quem executou o serviço.</b>

                            </p>



                            <p>

                                3.4. Assim, ao utilizar a <b>CARSUP</b>, o Usuário compreende e aceita que todas as suas

                                ações e

                                escolhas são livres e não possuem nenhum tipo de influência ou ingerência da

                                <b>CARSUP</b>.

                            </p>



                            <p>

                                3.5. Tanto os Usuários Clientes como os Usuários Prestadores de Serviço poderão ser

                                avaliados na plataforma, sendo certo que o emissor da avaliação será o único responsável

                                pelo conteúdo produzido.

                            </p>



                            <p>

                                <b> 4. CADASTRO DOS USUÁRIOS</b>

                            </p>

                            <p>

                                4.1. Para o cadastro e utilização dos serviços na plataforma da <b>CARSUP</b> serão

                                requeridas:



                            </p>

                            <p>

                                4.1.1. Informações Obrigatórias do Usuário Cliente: Nome completo, CPF, telefone celular

                                e e-mail. Modelo, marca e placa do veículo

                            </p>

                            <p>

                                4.1.1.1. Informações Opcionais do Usuário Cliente: Foto de perfil, data de nascimento,

                                gênero, foto do veículo e cor do veículo.

                            </p>



                            <p>

                                4.1.2. Informações Obrigatórias do Usuário Prestador de Serviço: Razão Social, CNPJ,

                                nome

                                do

                                responsável legal, e-mail, endereço, telefone e horário de funcionamento.

                            </p>



                            <p>

                                4.2. Cada Usuário determinará o seu login e senha de acesso, sendo de sua exclusiva

                                responsabilidade a manutenção do sigilo dessas informações. A <b>CARSUP</b> não se

                                responsabiliza

                                pelas ações e danos que poderão ser causados pelo acesso irregular da conta de acesso

                                por

                                terceiros.

                            </p>



                            <p>

                                4.3. É permitida a realização de apenas uma conta por Usuário, estando esta identificada

                                pelo CPF ou CNPJ.

                            </p>



                            <p>

                                4.4. O Usuário é responsável por fornecer informações verdadeiras, precisas e

                                atualizadas.

                            </p>

                            <p>

                                4.5. A <b>CARSUP</b> poderá recusar, suspender ou cancelar sem notificação prévia a

                                conta

                                de acesso

                                de um Usuário sempre que suspeitar que as informações fornecidas são falsas,

                                incompletas,

                                desatualizadas ou imprecisas ou, ainda, nos casos indicados nas leis e regulamentos

                                aplicáveis, nestes Termos de Uso ou em qualquer Política.

                            </p>



                            <p>

                                <b>5. FORMAS E CONDIÇÕES DE PAGAMENTO E CANCELAMENTO</b>

                            </p>

                            <p>

                                5.1. <b>PROCESSAMENTO DE PAGAMENTO:</b> O Usuário Cliente definirá o serviço, o Usuário

                                Prestador

                                de Serviço e a data na plataforma <b>CARSUP</b> e, posteriormente, os pagamentos serão

                                processados

                                pela plataforma independente Pagar.me, em que será possível escolher a forma de

                                pagamento

                                entre as opções disponíveis.

                            </p>

                            <p>

                                5.1.1. Ainda, será aceito o pagamento em dinheiro que deverá ser feito pelo Usuário

                                Cliente

                                no ato da entrega do serviço. Nesse caso, o Usuário Prestador de Serviços ficará com o

                                valor

                                da taxa da <b>CARSUP</b> em aberto e quando os repasses de pagamentos realizados por

                                meio da

                                plataforma forem feitos, a quantia em aberto será subtraída.

                            </p>

                            <p>

                                5.1.2. A <b>CARSUP</b> não possui acesso aos dados de cartão de crédito e demais dados

                                pessoais

                                inseridos na plataforma Pagar.me.

                            </p>

                            <p>

                                5.1.3. Assim, esclarece-se que estes Termos de Uso e Políticas de Privacidade são

                                exclusivos

                                da <b>CARSUP</b> e que recomendamos que o Usuário, antes de prosseguir com pagamentos,

                                leia e

                                aceite os Termos de Uso e Políticas de Privacidade da Pagar.me.

                            </p>



                            <p>

                                5.2. <b>VALORES COBRADOS:</b> No momento da contratação pelo Usuário Cliente será

                                cobrado

                                o valor

                                do serviço definido pelo Usuário Prestador de Serviço mais a taxa de 20% (vinte por

                                cento)

                                sobre o valor total da <b>CARSUP</b>.

                            </p>



                            <p>

                                5.3. <b>CANCELAMENTO E REEMBOLSO DE VALORES PAGOS:</b> Haverá devolução de valores pagos

                                pelos

                                Usuários Clientes nas seguintes hipóteses:

                            </p>



                            <p>

                                5.3.1. Em caso de exercício de direito de arrependimento por parte do Usuário Cliente: o

                                Código de Defesa do Consumidor prevê a possibilidade de cancelamento no prazo de 07

                                (sete)

                                dias, contados a partir da contratação. Nessas hipóteses, o Usuário Cliente será

                                reembolsado

                                integralmente pelos valores pagos.

                            </p>

                            <p>

                                <b>5.3.1.1. Assim, o Usuário Prestador de Serviço se torna ciente que os repasses dos

                                    valores

                                    pelos serviços prestados somente acontecerão se o serviço não for cancelado nos

                                    referidos 07

                                    (sete) dias. Caso o direito de arrependimento seja exercido, não será devida nenhuma

                                    quantia

                                    ao Usuário Prestador de Serviço.</b>

                            </p>



                            <p>

                                <b>5.3.2. Em caso de cancelamento após o prazo de 7 (sete) dias, será cobrada multa de

                                    25%

                                    (vinte e cinco por cento) sobre o valor total pago, pela prestação de serviços da

                                    <b>CARSUP</b> e

                                    como compensação pelo horário reservado.</b>

                            </p>



                            <p>

                                5.3.3. Em caso de cancelamento pelo Usuário Prestador de Serviço: o Usuário Cliente será

                                reembolsado por todos os valores pagos e o Usuário Prestador de Serviço deverá pagar as

                                taxas a serem pagas as operadoras.

                            </p>



                            <p>

                                5.3.4. Os reembolsos serão realizados pela mesma forma de pagamento utilizada na compra.



                            </p>



                            <p>

                                5.4. <b>EMISSÃO DE NOTAS FISCAIS:</b> As notas fiscais geradas pela <b>CARSUP</b> serão

                                referentes apenas

                                a suas taxas de serviços e, em nenhuma hipótese, sobre o valor total pago pelo Usuário

                                Cliente. O Usuário Prestador de Serviços é o único responsável pelo recolhimento dos

                                impostos de sua titularidade e pela geração de notas fiscais das quantias por ele

                                recebidas.

                            </p>



                            <p>

                                <b>6. RESPONSABILIDADE DAS PARTES</b>

                            </p>

                            <p>

                                6.1. Responsabilidades da <b>CARSUP</b>:

                            </p>

                            <p>

                                6.1.1. Realizar os serviços conforme o descrito nestes termos de uso;

                            </p>

                            <p>

                                6.1.2. Responsabilizar-se pelo funcionamento da plataforma e pelas correções que

                                eventualmente sejam necessárias;

                            </p>

                            <p>

                                6.1.3. Comunicar qualquer alteração dos serviços aos Usuários, por meio de comunicados

                                simples na plataforma;

                            </p>



                            <p>

                                <b>6.2. Responsabilidades do Usuário Cliente:</b>

                            </p>

                            <p>

                                6.2.1. Utilizar a plataforma conforme os critérios de utilização definidos pela

                                <b>CARSUP</b>, sem

                                alterar a sua programação, quebrar senhas ou realizar procedimentos que venham causar

                                prejuízos aos demais Usuários;

                            </p>

                            <p>

                                6.2.2. Responsabilizar-se para todos os efeitos, inclusive jurídicos, pelo teor das

                                informações que introduzir e pelos compromissos que assumir na plataforma;

                            </p>

                            <p>

                                6.2.3. Respeitar integralmente estes Termos de Uso, Políticas de Privacidade, legislação

                                vigente e contratos entre as partes.

                            </p>



                            <p>

                                <b>6.3. Responsabilidades do Usuário Prestador de Serviços:</b>

                            </p>

                            <p>

                                6.3.1. Utilizar a plataforma conforme os critérios de utilização definidos pela

                                <b>CARSUP</b>, sem

                                alterar a sua programação, quebrar senhas ou realizar procedimentos que venham causar

                                prejuízos aos demais Usuários ou aos clientes;

                            </p>

                            <p>

                                6.3.2. Responsabilizar-se para todos os efeitos, inclusive jurídicos, pelo teor das

                                informações e anúncios que introduzir na plataforma e pela qualidade e cumprimento

                                integral

                                dos serviços que se comprometer;

                            </p>

                            <p>

                                6.3.3. Respeitar integralmente estes Termos de Uso, Políticas de Privacidade, legislação

                                vigente e contratos entre as partes.

                            </p>



                            <p>

                                <b> 7. ISENÇÃO DE RESPONSABILIDADE DA CARSUP

                                </b>

                            </p>

                            <p>

                                7.1. Ao utilizar nossos serviços de intermediação os Usuários devem compreender e

                                concordar

                                em isentar a <b>CARSUP</b> de todos e quaisquer danos, perdas, responsabilidades e

                                despesas

                                resultantes de disputas entre os Usuários Clientes e os Usuários Prestadores de

                                Serviço.

                            </p>



                            <p>

                                7.2. Ainda, a <b>CARSUP não se responsabiliza por:</b>

                            </p>

                            <p>

                                7.2.1. Eventual indisponibilidade da plataforma, a qual não tenha dado causa.

                            </p>

                            <p>

                                7.2.2. Danos que o Usuário possa experimentar por ações exclusivas de terceiros, bem

                                como

                                falhas na conexão de rede e interações maliciosas como vírus.

                            </p>

                            <p>

                                7.2.3. Danos que o Usuário possa ter em decorrência do mau uso da plataforma em

                                desconformidade com estes Termos, com a Política de Privacidade, com a lei ou ordens

                                judiciais.

                            </p>

                            <p>

                                7.2.4. Casos fortuitos ou de força maior.

                            </p>



                            <p>

                                <b> 8. REGRAS DE CONDUTA E PROIBIÇÕES

                                </b>

                            </p>

                            <p>

                                8.1. Os Usuários <b>não</b> podem:

                            </p>

                            <p>

                                8.1.1. Lesar direitos da <b>CARSUP</b>, dos operadores da plataforma, de outros

                                Usuários, de

                                terceiros ou agir sob qualquer meio ou forma que possa contribuir com tal

                                violação;

                            </p>

                            <p>

                                8.1.2. Executar atos que limitem ou impeçam a utilização da plataforma ou

                                acessar

                                ilicitamente a <b>CARSUP</b>;

                            </p>

                            <p>

                                8.1.3. Utilizar a ferramenta para praticar ações ilícitas e difundir mensagens

                                não

                                relacionadas com a plataforma ou com suas finalidades, incluindo mensagens com

                                conteúdo

                                ilícito e impróprio;

                            </p>

                            <p>

                                8.1.4. Inserir dados que sejam falsos, desatualizados ou incompletos;

                            </p>

                            <p>

                                8.1.5. Responsabilizar a <b>CARSUP</b> por condutas de Usuários Prestadores

                                de Serviços ou Usuários

                                Clientes que estejam cadastrados na plataforma;

                            </p>



                            <p>

                                8.2. Ao sinal de qualquer descumprimento de conduta, o Usuário poderá ser

                                bloqueado ou

                                excluído da Plataforma, sem necessidade de aviso prévio.

                            </p>



                            <p>

                                <b>9. DA PROPRIEDADE INTELECTUAL </b>

                            </p>

                            <p>

                                9.1. A titularidade e os direitos relativos à plataforma da <b>CARSUP</b>

                                pertencem exclusivamente

                                à <b>CARSUP</b>. O acesso à plataforma e a sua regular utilização pelo

                                Usuário não lhe conferem

                                qualquer direito ou prerrogativa sobre propriedade intelectual ou outro

                                conteúdo nela

                                inserido.

                            </p>



                            <p>

                                9.2. Todo o conteúdo da plataforma, incluindo nome, marca, domínio,

                                programas, bases de

                                dados, arquivos, textos, desenhos, fotos, layouts, cabeçalhos e demais

                                elementos, foi

                                criado, desenvolvido ou cedido à <b>CARSUP</b>, sendo, portanto, de

                                propriedade exclusiva da <b>CARSUP</b>

                                ou a ela licenciado e encontra-se protegido pelas leis brasileiras e

                                tratados internacionais

                                que versam sobre direitos de propriedade intelectual.

                            </p>



                            <p>

                                9.3. São proibidas: a exploração, cessão, imitação, cópia, plágio, aplicação

                                de engenharia

                                reversa, tentativa de invasão (hackear), armazenamento, alteração,

                                modificação de

                                características, ampliação, venda, locação, doação, alienação,

                                transferência, reprodução,

                                integral ou parcial, de qualquer conteúdo da plataforma da <b>CARSUP</b>.



                            </p>



                            <p>

                                9.4. A pessoa que violar as proibições contidas na legislação sobre

                                propriedade intelectual

                                e nestes Termos será responsabilizada, civil e criminalmente, pelas

                                infrações cometidas,

                                além de poder ser penalizado na plataforma.

                            </p>



                            <p>

                                <b>10. TRATAMENTO DE DADOS PESSOAIS, PRIVACIDADE E SEGURANÇA </b>

                            </p>

                            <p>

                                A <b>CARSUP</b> dispõe de uma política específica para regular a coleta,

                                guarda e utilização de

                                dados pessoais, bem como a sua segurança: Política de Privacidade. Essa

                                política específica

                                integra inseparavelmente estes Termos, ressaltando-se que os dados de

                                utilização da

                                plataforma serão arquivados nos termos da legislação em vigor.

                            </p>



                            <p>

                                <b>11. DESDOBRAMENTOS DO ACESSO À PLATAFORMA </b>

                            </p>

                            <p>

                                11.1. Apesar dos melhores esforços da <b>CARSUP</b> para fornecer as

                                melhores

                                tecnologias para

                                manter a conexão e sincronização sempre online e acesso seguro aos Usuários,

                                em virtude de

                                dificuldades técnicas, aplicações de internet ou problemas de transmissão, é

                                possível

                                ocorrer cópias inexatas ou incompletas das informações contidas na

                                plataforma. Além disso,

                                vírus de computador ou outros programas danosos também poderão ser baixados

                                inadvertidamente

                                da plataforma.

                            </p>

                            <p>

                                11.1.1. A <b>CARSUP</b> recomenda a instalação de antivírus ou protetores

                                adequados.

                            </p>

                            <p>

                                11.2. A <b>CARSUP</b> se reserva o direito de unilateralmente modificar, a

                                qualquer momento e sem

                                aviso prévio, a plataforma, bem como a configuração, a apresentação, o

                                desenho, o conteúdo,

                                as funcionalidades, as ferramentas ou qualquer outro elemento, inclusive o

                                seu cancelamento.

                            </p>



                            <p>

                                <b>12. ALTERAÇÕES DOS TERMOS E CONDIÇÕES DE USO </b>

                            </p>

                            <p>

                                12.1. A <b>CARSUP</b> poderá unilateralmente adicionar e/ou modificar

                                qualquer cláusula contida

                                nestes Termos de Uso. A versão atualizada valerá para o uso da plataforma

                                realizado a partir

                                de sua publicação. A continuidade de acesso ou utilização da plataforma,

                                depois da

                                divulgação, confirmará a vigência dos novos Termos de Uso pelos Usuários.



                            </p>



                            <p>

                                12.2. Caso a mudança efetuada necessite de consentimento do Usuário, será

                                apresentada a

                                opção de aceitar de forma livre, inequívoca e informada o novo texto ou de

                                recusá-lo.

                            </p>



                            <p>

                                12.3. Caso o Usuário não esteja de acordo com a alteração, poderá não

                                fornecer

                                consentimento para atos específicos ou poderá rescindir totalmente seu

                                vínculo com a <b>CARSUP</b>.

                                Essa rescisão não eximirá, no entanto, o Usuário de cumprir com todas as

                                obrigações

                                assumidas sob as versões precedentes dos Termos de Uso.

                            </p>



                            <p>

                                <b>13. LEI APLICÁVEL E FORO DE ELEIÇÃO </b>

                            </p>

                            <p>

                                13.1. A Plataforma é controlada, operada e administrada na cidade de São

                                Paulo, Estado de

                                São Paulo, Brasil, podendo ser acessada por qualquer dispositivo conectado à

                                Internet,

                                independentemente de sua localização geográfica.

                            </p>

                            <p>

                                13.1.1. A <b>CARSUP</b> não garante que a plataforma seja apropriada ou

                                esteja disponível para uso

                                em outros locais. As pessoas que acessam ou usam a plataforma da

                                <b>CARSUP</b> a partir de outras

                                jurisdições o fazem por conta própria e são responsáveis pelo cumprimento

                                das leis

                                regionais/nacionais.

                            </p>



                            <p>

                                13.2. O Usuário concorda que a legislação aplicável para fins destes Termos

                                e

                                Condições de

                                Uso e das Políticas de Privacidade será a vigente na República Federativa do

                                Brasil.

                            </p>



                            <p>

                                13.3. A <b>CARSUP</b> e o Usuário concordam que o Foro de São Paulo, São

                                Paulo, Brasil, será o

                                único competente para dirimir qualquer questão ou controvérsia oriunda ou

                                resultante do uso

                                da plataforma, renunciando expressamente a qualquer outro, por mais

                                privilegiado que seja,

                                ou venha a ser.

                            </p>



                            <p>

                                <b>14. CONTATO </b>

                            </p>

                            <p>

                                A <b>CARSUP</b> disponibiliza os seguintes canais para receber todas as

                                comunicações que os

                                Usuários desejarem fazer: contato@carsup.com.br, chat on-line da plataforma

                                e telefones (11)

                                996084086 e (11) 933226552.

                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>


@endsection