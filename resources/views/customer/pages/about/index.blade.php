@extends('customer.templates.default')
@section('title', 'Quem Somos')

@section('description', 'Quem Somos')

@section('content')
<section class="content-pages company">

    <div class="container">

        <div class="row">

            <div class="col-sm-8">

                <div class="row">

                    <div class="col-sm-12">

                        <h3 class="title-section-pages">Quem Somos</h3>

                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-12">

                        <div class="box-text">

                            <p class="text-justify">

                                A CarsUp é uma plataforma digital que reúne os melhores centros de estética automotiva e

                                lava rápidos de cada região e os disponibiliza de acordo com suas preferências.

                                De forma simples e dinâmica por meio de filtros você pode encontrar o estabelecimento e

                                o serviço ideal para você e seu veiculo, ordenados por menor preço, melhor avaliado,

                                menor distância ou com o menor tempo para poder atendê-lo.

                                Você pode agendar e contratar um ou mais serviços dentre os diversos disponíveis na

                                plataforma e fazer o pagamento direto pelo app de forma segura e prática.

                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection