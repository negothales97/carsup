@extends('customer.templates.default')
@section('title', 'Fale Conosco')

@section('description', 'Fale Conosco')

@section('content')
<section class="steps">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="box-content-pages">

                    <div class="row">

                        <div class="col-md-12">

                            <h1>Fale conosco</h1>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-7">

                            <form action="#" method="POST">

                                <fieldset class="send-message">

                                    <legend>Enviar mensagem</legend>

                                    <div class="row">

                                        <div class="col-sm-6">

                                            <div class="form-group">

                                                <label for="name">Nome</label>

                                                <input type="text" id="name" class="form-control" name="name" placeholder="Ex: João Lima da Silva">

                                            </div>

                                        </div>

                                        <div class="col-sm-6">

                                            <div class="form-group">

                                                <label for="phone">Telefone</label>

                                                <input type="text" id="phone" class="form-control input-phone" name="phone" placeholder="Ex: (21) 92020-5030">

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-6">

                                            <div class="form-group">

                                                <label for="email">E-mail</label>

                                                <input type="email" id="email" class="form-control" name="email" placeholder="Ex: joaolima@gmail.com">

                                            </div>

                                        </div>

                                        <div class="col-sm-6">

                                            <div class="form-group">

                                                <label for="city">Cidade</label>

                                                <input type="text" id="city" class="form-control" name="city" placeholder="Ex: São Paulo">

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="form-group">

                                                <label for="message">Mensagem</label>

                                                <textarea id="message" class="form-control" name="message"

                                                    placeholder="Ex: Digite aqui a sua mensagem"></textarea>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-5">

                                            <div class="form-group">

                                                <button class="form-control btn-form" type="submit">Enviar

                                                    mensagem</button>

                                            </div>

                                        </div>

                                    </div>

                                </fieldset>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>


@endsection