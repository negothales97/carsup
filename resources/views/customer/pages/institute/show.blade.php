@extends('customer.templates.default')
@section('title', 'Estabelecimentos')

@section('description', 'Estabelecimentos')

@section('content')


    <section class="content-pages">

        <div class="container">

            <div class="row">

                <div class="col-sm-8">

                    <div class="logo-establishment">

                        <img src="{{ $institute->logoURL }}" alt="{{ $institute->name }}">

                    </div>

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="box-about-establishment">

                                <h3>{{ $institute->name }}</h3>

                                <p>{{ $institute->address->street }}, {{ $institute->address->number }}<br>
                                    {{ $institute->address->neighborhood }},
                                    {{ $institute->address->city }},
                                    {{ $institute->address->state }}<br>
                                    {{ $institute->address->zipCode }}
                                </p>

                            </div>

                        </div>

                    </div>
                    @php
                    //Columns must be a factor of 12 (1,2,3,4,6,12)
                    $numOfCols = 2;
                    $rowCount = 0;
                    $bootstrapColWidth = 12 / $numOfCols;
                    @endphp
                    <div class="row">
                        @foreach ($services as $service)
                            <div class="col-sm-col-sm-<?php echo $bootstrapColWidth; ?>">

                                <div class="box-service-establishment">

                                    <img src="{{ $service->service->photoURL }}" alt="{{$service->service->name}}">
                                    <h5>Lavagem Completa</h5>
                                    <p>
                                        Lavagem externa com shampoo e cera, aplicação de pretinho nos pneus, limpeza interna
                                        (Estofados e componentes), aspiração e lavagem de tapetes.
                                    </p>
                                </div>

                            </div>
                            @php
                            $rowCount++;
                            if ($rowCount % $numOfCols == 0){
                            echo ('</div><div class="row">');
                        }
                        @endphp
                        @endforeach

                    </div>

                </div>

                <div class="col-sm-4">

                    <div class="box-right">

                        <img id="img-shopping" src="{{ asset('images/shopping.png') }}" alt="Icon">

                        <h4>Contrate o serviço</h4>

                        <p>Para fazer a contratação de um serviço de um estabelecimento do site, baixe nosso app disponível
                            para Android e IOS.</p>

                        <div class="box-download">

                            <a href="https://apps.apple.com/br/app/carsup/id1528913762"><img class="download-ios"
                                    src="{{ asset('images/ios.png') }}" alt="IOS"></a>

                            <a href="https://play.google.com/store/apps/details?id=br.com.carsup"><img
                                    class="download-android" src="{{ asset('images/android.png') }}" alt="Android">

                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

@endsection
