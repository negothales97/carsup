@extends('customer.templates.default')
@section('title', 'Estabelecimentos')

@section('description', 'Estabelecimentos')

@section('content')

    <section class="content-pages">

        <div class="container">
            <div id="institute"></div>
        </div>

    </section>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            getInstitutes();
        });

        function getInstitutes() {
            let data = {};
            data.location = sessionStorage.getItem('location');
            var config = {
                method: 'post',
                url: `{{ url('api/institute') }}`,
                data
            };

            axios(config)
                .then(function(response) {
                    $('#institute').html(response.data);
                })
                .catch(function(error) {
                    console.log(error);
                });
        }

    </script>
@endsection
