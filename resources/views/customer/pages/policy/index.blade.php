@extends('customer.templates.default')
@section('title', 'Políticas de Privacidade')

@section('description', 'Políticas de Privacidade')

@section('content')
<section class="content-pages company">

    <div class="container">

        <div class="row">

            <div class="col-sm-8">

                <div class="row">

                    <div class="col-sm-12">

                        <div class="box-text">

                            <h3>Para você entender melhor o que fazemos com as suas informações</h3>

                            <p>

                                Fizemos o máximo para explicar de forma clara e simples quais dados pessoais

                                precisaremos

                                de

                                você e o que vamos fazer com cada um deles. Por isso, separamos abaixo os pontos mais

                                importantes, que também podem ser lidos de forma bem completa e detalhada nas nossas

                                <b>Políticas de Privacidade.</b>

                            </p>



                            <p>

                                Além disso, estamos sempre disponíveis para tirar qualquer dúvida que você tenha pelo

                                e-mail

                                contato@carsup.com.br, chat on-line da plataforma e telefones (11) 996084086 e (11)

                                933226552.

                            </p>

                            <p>

                                <b>1) </b>Para garantir a sua segurança, seus dados pessoais são transferidos de forma

                                criptografada e armazenados em servidores Linux, cujo acesso é restrito. Além disso,

                                utilizamos firewalls e o certificado SSL, que dispõe de uma variedade de tecnologias e

                                procedimentos de segurança para ajudar a proteger as informações dos Usuários. Com isso,

                                essas empresas passam a ter acesso aos seus dados pessoais, somente para armazená-los,

                                assim

                                que você os fornece na <b>CARSUP</b>. Se você tiver algum problema com isso, pedimos que

                                não

                                continue a utilização da plataforma.

                            </p>



                            <p>

                                <b>2) </b>Quando o Usuário Cliente se cadastrar na plataforma iremos pedir os seguintes

                                dados

                                obrigatórios: Nome completo, CPF, telefone celular e e-mail. Além disso, pediremos o

                                modelo,

                                marca e placa do veículo.

                            </p>



                            <p>

                                <b>3) </b>Se quiser, o Usuário Cliente pode nos fornecer: foto de perfil, data de

                                nascimento,

                                gênero, foto do veículo e cor do veículo.

                            </p>



                            <p>

                                <b>4) </b>Do Usuário Prestador de Serviços pediremos os seguintes dados obrigatórios:

                                Razão

                                Social,

                                CNPJ, nome do responsável legal, e-mail, endereço, telefone e horário de funcionamento.

                            </p>



                            <p>

                                <b>5) </b>Os dados bancários dos Usuários serão tratados pela plataforma independente

                                Pagar.me, que

                                se compromete a garantir a segurança das transações financeiras.

                            </p>



                            <p>

                                <b>6) </b>Nós compartilharemos os dados dos Usuários Clientes e dos veículos com os

                                Usuários

                                Prestadores de Serviço e vice e versa, para possibilitar a intermediação e o encontro

                                entre

                                as partes.

                            </p>



                            <p>

                                <b>7) </b>Quando você entra na nossa Plataforma, automaticamente são inseridos cookies

                                no

                                seu

                                navegador, e colhemos os seguintes dados de você: número de IP (Internet Protocol),

                                localização, duração da visita e páginas visitadas. Essas informações são obtidas sem

                                que

                                você tenha que preencher formulários e as coletamos porque temos uma obrigação definida

                                em

                                Lei de armazenar os seus registros de acesso por 06 (seis) meses. Além disso, nos ajuda

                                a

                                deixar a sua experiência melhor e mais personalizada.

                            </p>



                            <p>

                                <b>8) </b>Nós vamos armazenar também todas as conversas que você tiver conosco em nosso

                                chat

                                ou

                                nossos canais de comunicação, pois isso irá melhorar o seu atendimento e torná-lo mais

                                eficiente. Sem esse histórico, provavelmente todas as vezes que você utilizasse a

                                plataforma

                                teria que repetir o que já nos passou anteriormente.

                            </p>



                            <p>

                                <b>9) </b>Todos os seus dados são tratados com finalidades específicas e de acordo com a

                                Lei

                                de

                                Proteção de Dados Pessoais. Todas essas informações estão descritas em uma tabela, para

                                facilitar a sua visualização, nas nossas Políticas de Privacidade.

                            </p>



                            <p>

                                <b>10)</b> Mesmo que você já tenha nos fornecido seus dados pessoais, você possui total

                                direito de,

                                a qualquer momento: confirmar a existência de tratamento dos seus dados; acessar os seus

                                dados; corrigir os seus dados; tornar anônimo os dados; bloquear ou eliminar os dados

                                desnecessários, excessivos ou tratados em desconformidade com a Lei; pedir a

                                portabilidade

                                dos dados a outro fornecedor; eliminar dados, exceto aqueles exigidos por lei; obter

                                informação sobre quem a <b>CARSUP</b> realizou uso compartilhado de dados; obter

                                informação

                                sobre a

                                possibilidade de não fornecer consentimento e sobre as consequências da negativa; e

                                voltar

                                atrás e revogar o seu consentimento.

                            </p>



                            <p>

                                <b>11)</b> Nossa Política de Privacidade poderá mudar, mas você sempre poderá acessar a

                                versão mais

                                atualizada no nosso site. Além disso, se formos realizar alguma ação que a lei exija sua

                                autorização, você receberá um aviso antes para poder aceitar ou recusar.

                            </p>



                            <p>

                                <b>12)</b> A Política de Privacidade a seguir está dividida da seguinte forma para

                                facilitar

                                o seu

                                acesso à informação:

                            </p>

                            <ol type="a">

                                <li>Data de Disponibilização do Texto;</li>

                                <li>Explicação dos Termos Técnicos ou em Língua Estrangeira;</li>

                                <li>Privacidade do Usuário e Operadores de Dados Terceirizados;</li>

                                <li>Coleta de Dados;</li>

                                <li>Chat Online;</li>

                                <li>Finalidades e Bases Legais para o Tratamento de Dados Pessoais;</li>

                                <li>Compartilhamento dos Dados Pessoais dos Usuários;</li>

                                <li>Cancelamento da Plataforma, de Contas de Acesso e Exclusão de Dados; </li>

                                <li>Direitos do Titular dos Dados Pessoais;</li>

                                <li>Segurança;</li>

                                <li>Mudanças na Política de Privacidade;</li>

                                <li>Encarregado e Disposições Gerais;</li>

                                <li>Contato.</li>

                            </ol>

                            <h3>POLÍTICAS DE PRIVACIDADE</h3>



                            <p>

                                Antes de acessar a plataforma CARSUP, é importante que você leia, entenda e aceite de

                                forma

                                livre, inequívoca e informada estas Políticas de Privacidade.

                            </p>



                            <p>

                                Esta plataforma, cujo nome é CARSUP, é de propriedade, mantida e operada por ROSANA

                                GONCALVES CAPELLA 02161038869, pessoa jurídica de direito privado, inscrita no CNPJ sob

                                o nº

                                34.199.420/0001-60, com sede na Avenida Antônio Ricardo da Silva, 77, Jardim Nossa

                                Senhora

                                do Carmo, CEP 08.270-560, na Cidade de São Paulo, Estado de São Paulo, Brasil.

                            </p>



                            <p>

                                O presente documento visa prestar informações sobre a coleta, utilização, tratamento e

                                armazenamento dos dados pessoais fornecidos pelos Usuários e está em conformidade com a

                                Lei

                                12.965/2014 (Marco Civil da Internet) e com a Lei nº 13.709/18 (Lei Geral de Proteção de

                                Dados).

                            </p>



                            <h3> 1. DATA DE DISPONIBILIZAÇÃO DO TEXTO</h3>

                            <p>

                                O presente documento foi redigido e disponibilizado em 29/07/2019.

                            </p>



                            <h3> 2. EXPLICAÇÃO DOS TERMOS TÉCNICOS OU EM LÍNGUA ESTRANGEIRA</h3>

                            <p>

                                Abaixo estão dispostos os significados das nomenclaturas técnicas e termos na língua

                                inglesa:

                            </p>

                            <p>

                                Cookies: Pequenos arquivos de texto que ficam gravados no computador do internauta e

                                podem

                                ser recuperados pelo site que o enviou durante a navegação. São utilizados

                                principalmente

                                para identificar e armazenar informações sobre os visitantes.

                            </p>

                            <p>

                                Criptografia: Conjunto de princípios e técnicas para cifrar a escrita, torná-la

                                ininteligível para os que não tenham acesso às convenções combinadas.

                            </p>

                            <p>

                                Encarregado: Pessoa indicada pelo controlador e operador para atuar como canal de

                                comunicação entre o controlador, os titulares dos dados e a Autoridade Nacional de

                                Proteção

                                de Dados (ANPD).

                                IP (ou Internet Protocol): Identificação única para cada computador conectado a uma

                                rede.

                            </p>

                            <p>

                                Usuário Cliente: Pessoas físicas, externas ao sistema, que usufruem do serviço prestado

                                pela

                                plataforma, ao buscar empresas que prestam serviços para veículos.

                            </p>

                            <p>

                                Usuário Prestador de Serviço: Pessoas jurídicas, independentes e externas ao sistema,

                                que

                                oferecem serviços para veículos, como limpeza e estética automotiva, troca de filtros,

                                aplicação de insulfilme, troca de óleo e busca e entrega do veículo.

                            </p>

                            <p>

                                Usuários: Denominação em conjunto dos Usuários Clientes e Usuários Prestadores de

                                Serviços.

                            </p>



                            3. PRIVACIDADE DO USUÁRIO E OPERADORES DE DADOS TERCEIRIZADOS

                            <p>

                                3.1. Proteger sua privacidade é muito importante para nós. Seus dados são

                                transferidos de

                                forma criptografada e armazenados em servidores Linux, cujo acesso é restrito. Além

                                disso,

                                utilizamos firewalls e certificado de segurança SSL, dispondo de uma variedade de

                                tecnologias e procedimentos de segurança para ajudar a proteger as informações dos

                                Usuários.

                            </p>

                            <p>

                                3.2. Os servidores utilizados pela CARSUP que são munidos de mecanismos aptos a

                                assegurar a

                                segurança de seus dados estão localizados fora do Brasil. Dando aceite a estas

                                políticas, o

                                Usuário se declara ciente e de acordo com a transferência internacional de dados,

                                conforme

                                artigo 33, VIII da Lei n. 13709/18.¬¬

                            </p>

                            <p>

                                3.3. Os dados bancários serão tratados pela plataforma independente Pagar.me, que se

                                compromete a garantir a segurança das transações financeiras. Esclarece-se que estes

                                Termos

                                de Uso e Políticas de Privacidade são exclusivos da CARSUP e que recomendamos que o

                                Usuário,

                                antes de prosseguir com pagamentos, leia e aceite os Termos de Uso e Políticas de

                                Privacidade da Pagar.me.

                            </p>

                            <p>

                                3.4. Todos os registros de acesso, conjunto de informações referentes à data e hora

                                de uso

                                de uma determinada aplicação de internet a partir de um determinado endereço IP,

                                serão

                                mantidos pela CARSUP, sob sigilo, em ambiente controlado e de segurança, pelo prazo

                                mínimo

                                de 06 (seis) meses, nos termos da Lei n. 12.965/2014, e artigo 7º, II, da Lei

                                13709/18.

                            </p>

                            <p>

                                3.5. O Usuário deve se responsabilizar e se declarar exclusivo responsável por todas

                                as

                                ações, bem como por todas as informações e a veracidade do conteúdo que inserir na

                                plataforma.

                            </p>

                            <h3>4. COLETA DE DADOS</h3>

                            <p>

                                4.1. Dados Obrigatórios fornecidos pelo Usuário Cliente: Nome completo, CPF,

                                telefone

                                celular e e-mail, modelo, marca e placa do veículo.

                            </p>

                            <p>

                                4.1.1. Dados Opcionais fornecidos pelo Usuário Cliente: Foto de perfil, data de

                                nascimento,

                                gênero, foto do veículo e cor do veículo.

                            </p>

                            <p>

                                4.2. Dados Obrigatórios fornecidos pelo Usuário Prestador de Serviços: A CARSUP

                                solicita os

                                seguintes dados da empresa: Razão Social, CNPJ, nome do responsável legal, e-mail,

                                endereço,

                                telefone e horário de funcionamento.

                            </p>

                            <p>

                                4.3. Informações que coletamos de forma indireta: Coletamos informações de forma

                                indireta

                                através de cookies, como endereço IP utilizado, localização, tempos de visita e

                                outras

                                informações agregadas.

                            </p>

                            <p>

                                4.4. Histórico de contato: A CARSUP armazena informações a respeito de todas as

                                interações

                                já realizadas entre os Usuários por meio da plataforma, como mensagens em chat

                                online,

                                e-mails, contatos telefônicos e upload de arquivos, pois isso irá melhorar o seu

                                atendimento

                                e torná-lo mais eficiente. Sem esse histórico, provavelmente todas as vezes que você

                                utilizasse a plataforma teria que repetir o que já nos passou anteriormente.

                            </p>

                            <h3>5. CHAT ONLINE</h3>

                            <p>

                                5.1. Plataforma disponibiliza ferramenta própria para que os Usuários enviem

                                mensagens de

                                texto para a CARSUP, por meio da conexão com a Internet.

                            </p>

                            <p>

                                5.2. As conversas entre o Usuário e a CARSUP são armazenadas e monitoradas, com o

                                objetivo

                                de possibilitar as respostas aos Usuários e para futuros contatos.

                            </p>

                            <p>

                                5.3. A finalidade do chat online é fornecer uma ferramenta adicional para que os

                                Usuários

                                possam contatar a CARSUP e buscar maiores esclarecimentos sobre os serviços.

                            </p>

                            <p>

                                5.4. O Usuário deve estar ciente de que o conteúdo das mensagens é de exclusiva

                                responsabilidade do emissor.

                            </p>

                            <h3>6. FINALIDADES E BASES LEGAIS PARA O TRATAMENTO DE DADOS PESSOAIS</h3>

                            <p>

                                6.1. Ao aceitar a presente política de privacidade, o Usuário compreende que a

                                coleta e

                                tratamento dos dados pessoais abaixo são necessários para a execução do contrato com

                                a

                                CARSUP, conforme informações apresentadas a seguir:

                            </p>

                            <table class="table table-bordered">

                                <thead>

                                    <tr>

                                        <th scope="col">Tipo de Dado Pessoal</th>

                                        <th scope="col">Base Legal</th>

                                        <th scope="col">Finalidade</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    <tr>

                                        <td>Nome do Usuário Cliente

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018).</td>

                                        <td>Identificação do Usuário. Trata-se de dado pessoal essencial para que

                                            seja possível entrar em contato com o Usuário para atender às suas

                                            solicitações e fornecer respostas direcionadas. Assim como identificar o

                                            cliente no ato da prestação de serviço. </td>

                                    </tr>

                                    <tr>

                                        <td>CPF do

                                            Usuário Cliente

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018).</td>

                                        <td>É utilizado para identificar os Usuários na plataforma, além de gerar

                                            documentos necessários para a prestação do serviço, como notas fiscais.

                                        </td>

                                    </tr>

                                    <tr>

                                        <td>Telefone dos

                                            Usuários

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018).</td>

                                        <td>Será utilizado para contato com os Usuários, inclusive com envio de

                                            mensagens por whatsapp, caso necessário.</td>

                                    </tr>

                                    <tr>

                                        <td>E-mail dos

                                            Usuários

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018);</td>

                                        <td>Utilizado para login na plataforma, assim como meio de comunicação com o

                                            Usuário, para envio de notificações, atualizações recibos e promoções.

                                        </td>

                                    </tr>

                                    <tr>

                                        <td>Dados do Veículo do

                                            Usuário Cliente:

                                            modelo, marca e

                                            placa

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018).</td>

                                        <td>

                                            São utilizados para identificar em qual veículo o serviço será realizado

                                            e que tipos de serviços estão disponíveis.

                                        </td>

                                    </tr>

                                    <tr>

                                        <td>Foto de perfil,

                                            data de nascimento,

                                            gênero,

                                            foto e cor do veículo

                                            do Usuário Cliente

                                        </td>

                                        <td>Necessário para atender aos interesses legítimos do controlador ou de

                                            terceiro (Art. 7º, IX, Lei nº 13.709/2018).</td>

                                        <td>Serão utilizados para uma melhor experiência na plataforma. Tais

                                            informações são opcionais e só são obtidas quando, de forma livre o

                                            Usuário as insere espontaneamente na plataforma. </td>

                                    </tr>

                                    <tr>

                                        <td>Nome do responsável

                                            legal do Usuário

                                            Prestador de

                                            Serviços

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018).</td>

                                        <td>Necessário para comunicação com o Usuário Prestador de Serviço, para

                                            verificação de legitimidade para contratar, para atender às solicitações

                                            e fornecer respostas direcionadas. </td>

                                    </tr>

                                    <tr>

                                        <td>Endereço atual e

                                            Localização

                                        </td>

                                        <td>Necessário para a execução de contrato ou de procedimentos preliminares

                                            relacionados a contrato do qual seja parte o titular, a pedido do

                                            titular dos dados (Art. 7º, V, Lei nº 13.709/2018).</td>

                                        <td>Para fins de execução da prestação de serviços, para que o Usuário

                                            Prestador de Serviço possa buscar e entregar o veículo, caso seja

                                            contratado para esse fim, assim como para apontar os estabelecimentos

                                            mais próximos e serviços que atendem no local.</td>

                                    </tr>

                                    <tr>

                                        <td>IP (Internet Protocol),

                                            Localização,

                                            Fonte de referência,

                                            Tipo de navegador,

                                            Duração da visita,

                                            Páginas visitadas



                                        </td>

                                        <td>Cumprimento de obrigação legal ou regulatória pelo controlador (Art. 7º,

                                            II, Lei nº 13.709/2018).</td>

                                        <td>Obediência ao artigo 15 da Lei n. 12.965/2014, que impõe o dever da

                                            <b>CARSUP</b> de manter os respectivos registros de acesso a aplicações de

                                            internet, sob sigilo, em ambiente controlado e de segurança, pelo prazo

                                            de 6 (seis) meses</td>

                                    </tr>

                                </tbody>

                            </table>

                            <h3>7. COMPARTILHAMENTO DOS DADOS PESSOAIS DOS USUÁRIOS</h3>

                            <p>

                                7.1. Poderão ter acesso interno as informações dos Usuários Clientes apenas pessoas

                                estritamente necessárias para a prestação do serviço.

                            </p>

                            <p>

                                7.2. Os dados do Usuário Cliente serão compartilhados pela <b>CARSUP</b> com o Usuário

                                Prestador de

                                Serviços, assim como os dados da empresa Prestadora de Serviços serão compartilhados com

                                o

                                Usuário Cliente. O objetivo desse compartilhamento é exclusivamente possibilitar a

                                intermediação contratada por meio da plataforma. Assim, o Usuário que tiver acesso a

                                dados

                                pessoais de terceiros deve se responsabilizar pela segurança e pelo tratamento adequado

                                dessas informações, não podendo divulga-las em desconformidade com a legislação vigente

                                e

                                com esta Política de Privacidade.

                            </p>

                            <p>

                                7.3. A <b>CARSUP</b> não compartilhará com terceiros os dados sigilosos dos Usuários,

                                salvo por

                                força de lei ou ordem judicial.

                            </p>

                            <h3>8. CANCELAMENTO DA PLATAFORMA, DE CONTAS DE ACESSO E EXCLUSÃO DE DADOS</h3>

                            <p>

                                8.1. Cancelamento de contas de acesso pela <b>CARSUP</b>: A <b>CARSUP</b> poderá, a seu

                                exclusivo

                                critério, bloquear, restringir, desabilitar ou impedir o acesso de qualquer Usuário à

                                plataforma sempre que for detectada uma conduta inadequada.

                            </p>

                            <p>

                                8.2. Cancelamento de contas de acesso pelo Usuário: Os Usuários que queiram podem

                                solicitar

                                diretamente o cancelamento da conta (login), por meio do e-mail indicado no item

                                “Contato”.

                            </p>

                            <p>

                                8.3. Exclusão de dados: No momento do cancelamento da conta, quando finda a finalidade

                                de

                                tratamento de dados ou diante de solicitação, o Usuário terá todos os seus dados

                                deletados

                                imediatamente e permanentemente da plataforma, exceto os registros de acesso (conjunto

                                de

                                informações referentes à data e hora de uso de uma determinada aplicação de internet a

                                partir de um determinado endereço IP), que serão mantidos, sob sigilo, em ambiente

                                controlado e de segurança, pelo prazo de 6 (seis) meses, nos termos da Lei n.

                                12.965/2014 e

                                com a base legal do art. 7, II, da Lei Geral de Proteção de Dados Pessoais.

                            </p>

                            <h3>9. DIREITOS DO TITULAR DOS DADOS</h3>

                            <p>

                                9.1. O titular dos dados tem direito a obter da <b>CARSUP</b>, em relação aos dados por

                                ela

                                tratados, a qualquer momento e mediante requisição:

                            </p>

                            <p>

                                9.1.1. Confirmação da existência de tratamento de dados;

                            </p>

                            <p>

                                9.1.2. Acesso aos dados;

                            </p>

                            <p>

                                9.1.3. Correção de dados incompletos, inexatos ou desatualizados;

                            </p>

                            <p>

                                9.1.4. Anonimização, bloqueio ou eliminação de dados desnecessários, excessivos ou

                                tratados

                                em desconformidade com o disposto na Lei 13.709/2018;

                            </p>

                            <p>

                                9.1.5. Portabilidade dos dados a outro fornecedor de serviço ou produto, mediante

                                requisição

                                expressa e observados os segredos comercial e industrial;

                            </p>

                            <p>

                                9.1.6. Eliminação dos dados tratados com o consentimento do titular, exceto nas

                                hipóteses

                                previstas na Lei 13.709/2018;

                            </p>

                            <p>

                                9.1.7. Informação das entidades públicas e privadas com as quais a <b>CARSUP</b>

                                realizou uso

                                compartilhado de dados;

                            </p>

                            <p>

                                9.1.8. Informação sobre a possibilidade de não fornecer consentimento e sobre as

                                consequências da negativa;

                            </p>

                            <p>

                                9.1.9. Revogação do consentimento.

                            </p>

                            <h3>10. SEGURANÇA</h3>

                            <p>

                                A <b>CARSUP</b> tem o compromisso de preservar a estabilidade, segurança e

                                funcionalidade da

                                plataforma, por meio de medidas técnicas compatíveis com os padrões internacionais e

                                pelo

                                estímulo ao uso de boas práticas. Todavia nenhum serviço disponível na internet possui

                                total

                                garantia contra invasões ilegais. Em casos em que terceiros não autorizados invadam o

                                sistema de forma ilícita, a <b>CARSUP</b> diligenciará da melhor forma para encontrar o

                                responsável

                                pela atuação ilícita, mas não se responsabiliza pelos danos por eles causados.

                            </p>

                            <h3>11. MUDANÇAS NA POLÍTICA DE PRIVACIDADE</h3>

                            <p>

                                11.1. A <b>CARSUP</b> poderá unilateralmente adicionar e/ou modificar qualquer cláusula

                                contida

                                nestas Políticas de Privacidade. A versão atualizada valerá para o uso da plataforma

                                realizado a partir de sua publicação. A continuidade de acesso ou utilização do site,

                                depois

                                da divulgação, confirmará a vigência das novas Políticas de Privacidade pelos Usuários.

                            </p>

                            <p>

                                11.2. Caso a mudança efetuada necessite de consentimento do Usuário, será apresentada a

                                opção de aceitar de forma livre, inequívoca e informada o novo texto ou de recusá-lo.

                            </p>

                            <p>

                                11.3. Caso o Usuário não esteja de acordo com a alteração, poderá não fornecer

                                consentimento

                                para atos específicos ou poderá rescindir totalmente seu vínculo com a <b>CARSUP</b>.

                                Essa rescisão

                                não eximirá, no entanto, o Usuário de cumprir com todas as obrigações assumidas sob as

                                versões precedentes das Políticas de Privacidade.

                            </p>

                            <h3>12. ENCARREGADO E DISPOSIÇÕES GERAIS</h3>

                            <p>

                                12.1. A <b>CARSUP</b> indica como encarregado o(a) Sr(a). Ricardo Gonçalves de Moura,

                                inscrito no

                                CPF sob o número 355.009.798-09, com endereço eletrônico ricardomoura@carsup.com.br, nos

                                termos do art. 41 da Lei Geral de Proteção de Dados, para aceitar reclamações e

                                comunicações

                                dos titulares e da Autoridade Nacional de Proteção de Dados, prestar esclarecimentos e

                                adotar providências.

                            </p>

                            <p>

                                12.2. A <b>CARSUP</b> dispõe de um texto específico para regular a licença de uso, os

                                direitos,

                                deveres, garantias e disposições gerais: os Termos de Uso. Todos esses documentos

                                integram

                                inseparavelmente estas Políticas de Privacidade.

                            </p>

                            <h3>13. CONTATO</h3>

                            <p>

                                A <b>CARSUP</b> disponibiliza os seguintes canais para receber todas as comunicações que

                                os

                                Usuários desejarem fazer: contato@carsup.com.br, chat on-line da plataforma e telefones

                                (11)

                                996084086 e (11) 933226552.

                            </p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection