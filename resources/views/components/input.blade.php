<div class="form-group">
    <label for="{{$name}}">{{$label}}</label>
    <input type="{{$type}}" id="{{$name}}" class="form-control {{$clazz}} {{ $errors->has($name) ? 'is-invalid' : '' }}"
        name="{{$name}}" value="{{ old($name) }}" placeholder="{{$placeholder}}">
    @if ($errors->has($name))
        <span class="help-block">
            <small>{{ $errors->first($name) }}</small>
        </span>
    @endif
</div>
