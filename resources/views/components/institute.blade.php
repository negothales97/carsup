@php
//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 3;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
@endphp
<div class="row">
    @foreach ($institutes as $institute)
        <div class="col-sm-<?php echo $bootstrapColWidth; ?>">
            <a href="{{ route('institute.show', ['resourceId' => $institute->id]) }}">

                <div class="box-establishment">
                    <div class="row">
                        <div class="col-sm-5">
                            <img src="{{ $institute->logoURL }}" alt="{{ $institute->name }}">
                        </div>
                        <div class="col-sm-7">
                            <h4>{{ $institute->name }}</h4>
                            <p id="about-best">
                                {{-- <span><i class="fa fa-star" aria-hidden="true"></i> {{ $institute->rating }}</span> • --}}<br>
                                {{-- Lava-rápido• --}}
                                {{ convertToKm($institute->distance) }} km de você
                            </p>
                            {{-- <p id="time-best">
                                50-60 min
                                •
                                R$ 4,40
                            </p> --}}
                        </div>
                    </div>
                </div>
            </a>

        </div>
        @php
        $rowCount++;
        if ($rowCount % $numOfCols == 0){
            echo ('</div><div class="row">');
        }
    @endphp
    @endforeach
</div>
