<!doctype html>
<html>

<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Cadastro Parceiro</title>
</head>

<body class="">

    <div>
        <h3>Dados do titular</h3>
        <p><b>Nome:</b>{{ $data['owner']['firstName'] }}</p>
        <p><b>Sobrenome:</b>{{ $data['owner']['lastName'] }}</p>
        <p><b>E-mail:</b>{{ $data['owner']['email'] }}</p>
        <p><b>Telefone:</b>{{ $data['owner']['phone'] }}</p>
        <p><b>RG:</b>{{ $data['owner']['rg'] }}</p>
        <p><b>Orgão Emissor:</b>{{ $data['owner']['organ'] }}</p>
        <p><b>CPF:</b>{{ $data['owner']['cpf'] }}</p>
        <p><b>Aceitou Contato
                Whatsapp:</b>{{ $data['owner']['aceitouContatoWhats'] }}</p>

    </div>
    <br><br><br>
    <div>
        <h3>Dados do Lava Rápido</h3>
        <p><b>Nome:</b>{{ $data['carWash']['name'] }}</p>
        <p><b>Razão Social:</b>{{ $data['carWash']['socialName'] }}</p>
        <p><b>Telefone:</b>{{ $data['carWash']['phone'] }}</p>
        <p><b>CNPJ:</b>{{ $data['carWash']['cnpj'] }}</p>

        <p><b>CEP:</b>{{ $data['carWash']['cep'] }}
        </p>
        <p><b>Logradouro:</b>{{ $data['carWash']['street'] }}
        </p>
        <p><b>Número:</b>{{ $data['carWash']['number'] }}
        </p>
        <p><b>Complemento:</b>{{ $data['carWash']['complement'] }}
        </p>
        <p><b>Bairro:</b>{{ $data['carWash']['neighborhood'] }}
        </p>
        <p><b>Cidade:</b>{{ $data['carWash']['city'] }}
        </p>
        <p><b>Estado:</b>{{ $data['carWash']['uf'] }}
        </p>

    </div>
    <br><br><br>
    <div>
        <h3>Dados Bancários</h3>
        <p><b>Banco:</b>{{ $data['bank']['bank'] }}</p>
        <p><b>Agência (sem dígito):</b>{{ $data['bank']['agency'] }}</p>
        <p><b>Conta:</b>{{ $data['bank']['account'] }}</p>
        <p><b>Dígito:</b>{{ $data['bank']['digit'] }}</p>
        <p><b>CNPJ:</b>{{ $data['bank']['cnpj'] }}</p>
        <p><b>Tipo de
                Conta:</b>{{ $data['bank']['type_count']== 'corrente' ? 'Conta Corrente' : 'Conta Poupança' }}
        </p>

    </div>
    <br><br><br>
    <div>
        <h3>Dados Finais</h3>
        <p><b>Senha de login no painel:</b>{{ $data['other']['password'] }}</p>
        <p><b>Lavagem por Mês:</b>{{ $data['other']['qtdLavaPorMes'] }}</p>
        <p><b>Possui Seguro:</b>{{ $data['other']['seguroPossui'] }}</p>
        <p><b>Fornecedor de produtos de
                limpeza:</b>{{ $data['other']['productClean'] }}</p>
        @if (isset($data['other']['seguroPossui']) && $data['other']['seguroPossui'] == "true")
            <p><b>Valor de cobertura:</b>{{ $data['other']['productClean'] }}</p>
        @endif
    </div>
</body>

</html>
