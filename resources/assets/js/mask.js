
$(".input-document").focusout(function() {
    var tamanho = $(".input-document").val().length;

    if (tamanho < 14) {
        $(".input-document").inputmask("999.999.999-99");
    } else {
        $(".input-document").inputmask("99.999.999/9999-99");
    }
});
$(".input-document").focusin(function() {
    $(".input-document").inputmask('remove');
});
$('.input-cep').inputmask({
    "mask": "99999-999",
    "placeholder": "_"
});
$('.input-cpf').inputmask({
    "mask": "999.999.999-99",
    "placeholder": "_"
});

$('.input-cnpj').inputmask({
    "mask": "99.999.999/9999-99",
    "placeholder": "_"
});

$('.input-phone').on('focusout click', function() {
    var phone = $(this).val().replace(/\D/g, '');
    if (phone.length > 10) {
        $(this).inputmask({
            "mask": "(99) 99999-9999",
            "placeholder": " "
        });
    } else {
        $(this).inputmask({
            "mask": "(99) 9999-99999",
            "placeholder": " "
        });
    }
});
$(".input-money").maskMoney({
    thousands: '.',
    decimal: ',',
    allowZero: true,
    symbolStay: true
});
